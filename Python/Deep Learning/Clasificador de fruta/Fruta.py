from sklearn import tree

# 1 = suave
# 0 = rugoso
# caracteristicas [i][0] es el peso
# caracteristicas [i][0] es la rugosidad
caracteristicas = [[140, 1], [130, 1], [150, 0], [170, 0]]
# Son los resultados de si los datos de arriba son verdaderos o falsos
# 0 si es una manzana, 1 si es una naranja
labels = [0, 0, 1, 1]

clf = tree.DecisionTreeClassifier ()
clf = clf.fit (caracteristicas, labels)

print (clf.predict ([[150, 0]]))