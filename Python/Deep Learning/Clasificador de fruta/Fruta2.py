from sklearn import tree

# 1 = suave
# 0 = rugoso
# caracteristicas [i][0] es el peso
# caracteristicas [i][0] es la rugosidad
caracteristicas = [[140, True], [130, True], [150, False], [170, False]]
# Son los resultados de si los datos de arriba son verdaderos o falsos
# 0 si es una manzana, 1 si es una naranja
labels = ['manzana', 'manzana', 'naranja', 'naranja']

clf = tree.DecisionTreeClassifier ()
clf = clf.fit (caracteristicas, labels)

resultados = clf.predict ([[12, False]])
print ('Es una', resultados [0])