import numpy
from sklearn import tree
from sklearn.datasets import load_iris
from six import StringIO
import pydot
import graphviz

iris = load_iris ()

test_idx = [0, 50, 100]

# Datos para entrenar
objetivo_del_entrenamiento = numpy.delete (iris.target, test_idx)
datos_del_entrenamiento = numpy.delete (iris.data, test_idx, axis = 0)

# Datos para comprobar
objetivo_del_testeo = iris.target [test_idx]
datos_del_testeo = iris.data [test_idx]

clasificador = tree.DecisionTreeClassifier ()
clasificador.fit (datos_del_entrenamiento, objetivo_del_entrenamiento)

print (objetivo_del_testeo)
print (clasificador.predict (datos_del_testeo))

# dot_data = StringIO ()
# tree.export_graphviz (clasificador, out_file = dot_data, feature_names = iris.feature_names, class_names = iris.target_names, filled = True, rounded = True, impurity = False)

# grafico = graphviz.Source (dot_data.getvalue ())
# grafico.render ('iris', view = True)