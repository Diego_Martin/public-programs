import socket
from _thread import *
from objetos import *
import pickle
import pygame
import random
import sys

Apagar = False
server = str(socket.gethostbyname_ex(socket.gethostname())[2][1])
puerto = 65300

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((server, puerto))
except socket.error as e:
    str(e)

s.listen(10)
print("Esperando la conexion, servidor encendido")

jugadores = []
for js in range(10):
    jugadores.append(Jugador(random.randint(10,390), random.randint(10,390), (random.randint(0,255), random.randint(0,255), random.randint(0,255)), 10, (pygame.K_w, pygame.K_s, pygame.K_a, pygame.K_d), False))

puntos = []
for i in range(10):
    puntos.append(Punto(random.randint(10, 390), random.randint(10, 390), (random.randint(0,255), random.randint(0,255), random.randint(0,255))))
    
def abrirCliente(conn, cliente):
    conn.send(pickle.dumps([jugadores, puntos, cliente]))
    # jugadores[cliente].visible = True
    respuesta = ""

    while True:
        print(cliente)
        try:
            data = pickle.loads(conn.recv(4096))
            jugadores[cliente] = data[0][cliente]
            # print(jugadores[cliente])
            print(data)

            if not(data):
                print("Desconectado")
                break
            else:
                respuesta = [jugadores, puntos, cliente]

                print("Recibido: ", data)
                print("Enviando: ", respuesta)
            conn.sendall(pickle.dumps(respuesta))
        except:
            break
    print("Conexion perdida")
    conn.close()
    sys.exit()


clientesConectados = 0
while True:
    conn, direccion = s.accept()
    print("Conectado a: ", direccion)

    start_new_thread(abrirCliente, (conn, clientesConectados))

    jugadores[clientesConectados].visible = True
    clientesConectados += 1