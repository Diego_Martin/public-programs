import socket
import pickle

class Sala():
    def __init__(self):
        self.cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server = str(socket.gethostbyname_ex(socket.gethostname())[2][1])
        self.puerto = 65300
        self.direccion = (self.server, self.puerto)
        self.p = self.conectar()
    
    def conectar(self):
        try:
            self.cliente.connect(self.direccion)
            return pickle.loads(self.cliente.recv(4096))
        except:
            pass

    def getPos(self):
        return self.p

    def enviar(self, datos):
        if datos[2] >= 1:
            try:
                self.cliente.send(pickle.dumps(datos))
                return pickle.loads(self.cliente.recv(4096))
            except socket.error as e:
                print(e)
        else:
            return "Esperando por jugadores"