import pygame
import random
class Jugador(object):
    def __init__(self, x, y, color=(255,0,0), radio=10, teclas=[], visible=False):
        self.x = x
        self.y = y
        self.color = color
        self.radio = radio
        self.teclas = teclas
        self.puntuacion = 0
        self.visible = visible
    
    def dibujar(self, ven):
        if self.visible:
            pygame.draw.circle(ven, self.color, (self.x, self.y), self.radio)
            pygame.draw.circle(ven, (0,0,0), (self.x, self.y), self.radio, 2)
    
    def teclass(self):
        tecla = pygame.key.get_pressed()
        if tecla[self.teclas[0]]:
            if (self.y - 2) > self.radio:
                self.y -= 2
        
        if tecla[self.teclas[1]]:
            if (self.y + 2) < (400 - self.radio):
                self.y += 2

        if tecla[self.teclas[2]]:
            if (self.x - 2) > self.radio:
                self.x -= 2

        if tecla[self.teclas[3]]:
            if (self.x + 2) < (400 - self.radio):
                self.x += 2

class Punto(object):
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.color = color
        self.radio = 5

    def dibujar(self, ven):
        pygame.draw.circle(ven, self.color, (self.x, self.y), self.radio)

    def coger(self):
        self.x = random.randint(10, 390)
        self.y = random.randint(10, 390)
        print("Tocado")