# Asistentes virtuales

la idea trata de crear un asistente virtual que me ayude con diversas tareas o me dé cierta información útil.


Es una idea que tuve en el verano de 2019 que fue inicialmente pensada para ayudarme a programar pero hoy en día está orientada a imitar a Google Home, Amazon Alexa o Apple Siri.


En la carpeta "Jarvis" puede encontrar la versión más reciente del asistente, que se conecta a una base de datos que se aloja en mi servidor personal.
En la carpeta "pruebas_asistente" se pueden encontrar versiones más antiguas o versiones fallidas.