import pyttsx3
import random
import speech_recognition as escuchador

reconoce = escuchador.Recognizer()
salir = False
# estasFijandoUnaVariable = False
aceptar = ["vale", "si, amo"]
para_salir = ["exit", "adios", "bye", "go home", "apagate"]
me_llaman =  ["vega", "hola", "hello", "hi"]
engine = pyttsx3.init('sapi5')

def hablar(texto):
    engine.say(texto)
    engine.runAndWait()

while not(salir):
    with escuchador.Microphone() as source:
        # hablar("Dime algo bonito")
        # print("Funciona")
        audio = reconoce.listen(source)

        texto = reconoce.recognize_google(audio)
        if texto != "":
            try:
                

                # hablar("Has dicho: " + texto)
                print("Has dicho: " + texto)
                # Llamo a Vega
                if texto.lower() in me_llaman:
                    hablar("Si?")
                # Si el texto es alguno de la lista "para_salir", sale del bucle
                if texto.lower() in para_salir:
                    hablar(random.choice(aceptar))
                    salir = True
                
                # Intento de crear una variable.
                # if estasFijandoUnaVariable == "3":
                #     nueva_var += " = " + texto.lower()
                #     hablar("variable creada con exito")
                #     hablar("mostrando por consola")
                #     print(nueva_var)
                # if estasFijandoUnaVariable == "2":
                #     nueva_var = texto.lower()
                #     hablar("di el valor de la variable")
                #     estasFijandoUnaVariable = "3"
                # if texto.lower() == "dog":
                #     hablar("di el nombre de la variable")
                #     estasFijandoUnaVariable = "2"

            except:
                print("No te entiendo.")

hablar("Hasta la proxima.")