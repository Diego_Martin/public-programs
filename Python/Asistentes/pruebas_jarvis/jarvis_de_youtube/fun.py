def letraANumero(numero):
    res = ""
    if numero == "cero" or numero == "0":
        res += "0"
    if numero == "uno" or numero == "1":
        res += "1"
    if numero == "dos" or numero == "2":
        res += "2"
    if numero == "tres" or numero == "3":
        res += "3"
    if numero == "cuatro" or numero == "4":
        res += "4"
    if numero == "cinco" or numero == "5":
        res += "5"
    if numero == "seis" or numero == "6":
        res += "6"
    if numero == "siete" or numero == "7":
        res += "7"
    if numero == "ocho" or numero == "8":
        res += "8"
    if numero == "nueve" or numero == "9":
        res += "9"
    
    return int(res)
