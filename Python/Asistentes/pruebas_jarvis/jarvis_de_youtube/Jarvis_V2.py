import pyttsx3 # pip install pyttsx3
import speech_recognition as sr # pip install speechRecognition
import datetime
import os
import random
import re
import time
import smtplib
import unicodedata
import webbrowser # pip install webbrowser
import wikipedia # pip install wikipedia
import pyautogui # pip install pyautogui
from twilio.rest import Client as WClient # pip install twilio (para WhatsApp)
import requests
import pymysql # pip install pymysql
from conexion import Conexion as JConexion
from fun import *
import pyql # pip install pyql
from pyql.geo.placefinder import PlaceFinder
import geopy # pip install geopy

ConBBDD = JConexion()
ConBD = ConBBDD.ConexionMySQL()
print(ConBD)
if ConBD == None:
    pyautogui.press("win")
    time.sleep(0.5)
    pyautogui.typewrite("XAMPP", interval=0.2)
    pyautogui.press("enter")
    time.sleep(2.0)
    # print(list(pyautogui.locateAllOnScreen("./jarvis_del_tio_de_youtube/necesario/startXampp.PNG")))
    for pos in pyautogui.locateAllOnScreen("./jarvis_del_tio_de_youtube/necesario/startXampp2.PNG"):
        print(pos)
        pyautogui.moveTo(x=pos.left, y=pos.top, duration=1.0)
        pyautogui.click()
        pyautogui.moveTo(x=pos.left, y=(pos.top + 60), duration=1.0)
        pyautogui.click()

    pyautogui.keyDown("win")
    pyautogui.press("down")
    pyautogui.keyUp("win")

salir = False
engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
# engine.setProperty('voices', voices[0].id)
engine.setProperty('voices', 'spanish')

def datosLugar():
    geolocator = geopy.geocoders.Nominatim(user_agent="Jarvis_V1")
    location = geolocator.geocode("175 5th Avenue NYC")
    print((location.latitude, location.longitude))
    lat = location.latitude
    lng = location.longitude
    lat_lng = "{0}, {1}".format(lat, lng)
    buscador = PlaceFinder.get(text=lat_lng, gflags="R")
    infoLugar = "{0}, {1} ZIP: {2} | WOEID:{3}".format(buscador.city, buscador.country, buscador.uzip, buscador.woeid)
    print(infoLugar)

def aceptar():
    decir(random.choice(["vale, señor", "si, amo", "por supuesto, señor", "si, señor", "si, jefe"]))

def chiste():
    espacio = "                                            "
    chistes = ["¿Por qué las focas del circo miran siempre hacia arriba?" + espacio + "Porque es donde están los focos.", "¡Estás obsesionado con la comida!" + espacio + "No sé a que te refieres croquetamente.", "¿Por qué estás hablando con esas zapatillas?" + espacio + "Porque pone 'converse'", "¿Sabes cómo se queda un mago después de comer?" + espacio + "Magordito", "Me da un café con leche corto." + espacio + "Se me ha roto la máquina, cambio.", "Buenos días, me gustaría alquilar 'Batman Forever'." + espacio + "No es posible, tiene que devolverla tumorrou.", "¡Camarero! Este filete tiene muchos nervios." + espacio + "Normal, es la primera vez que se lo comen.", "¿Qué le dice un techo a otro?" + espacio + "Techo de menos."]
    decir(random.choice(chistes))
    os.startfile("D:\\Programas\\Python\\jarvis_del_tio_de_youtube\\necesario\\xd.mp3")
    pyautogui.moveTo(x=1910, y=5, duration=3.0)
    time.sleep(0.5)
    pyautogui.click()

def escribirMensaje():
    decir("Dictame lo que debo escribir")
    textoW = comando().capitalize()
    textoW = textoW + "."
    pyautogui.typewrite(message=textoW, interval=0.1)
    decir("Quieres enviarlo ya?")
    confirmar = comando()
    if "si" in confirmar:
        try:
            coords = list(pyautogui.center(pyautogui.locateOnScreen("./jarvis_del_tio_de_youtube/necesario/enviaWasap.PNG")))
            pyautogui.moveTo(x=coords[0], y=coords[1], duration=1.0)
            pyautogui.click()
        except Exception as equis:
            decir("No encuntro el boton: enviar")
            print(equis)
    else:
        escribirMensaje()

def salir_en_query(query):
    para_salir = ["exit", "adios", "salir", "apagate"]
    if len(query) > 0:
        for x in range(len(para_salir)):
            if para_salir[x] in query:
                return True
        return False
    else:
        return False

def decir(audio):
    engine.say(audio)
    engine.runAndWait()

def sendEmail(persona, contenido):
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login("[correo]", "[pass]")
    server.sendmail("[correo]", persona, contenido)
    server.close()

def obtenerTelefono():
    ext = input("Escribe la extension de tu telefono: ")
    de = input("Escribe el numero de telefono: ")
    de = de.replace(" ", "")
    ext = ext.replace("+", "")
    de = "whatsapp:+" + ext + de
    print(de)
    return de

def enviameWhas(de="whatsapp:+14155238886", para="whatsapp:+34[Numero de telefono]", mensaje="Mensaje predeterminado"):
    sid = "AC793b69716ab1002a3fb51374a54b3f33"
    token = "fe57ee8f4dfa1e9ab41dd99911a2ff22"
    cliente = WClient(sid, token)
    cliente.messages.create(body=mensaje, from_=de, to=para)

def saluda():
    hora = int(datetime.datetime.now().hour)
    if hora >= 0 and hora < 13:
        decir("Buenos dias, señor")
    elif hora >= 13 and hora < 18:
        decir("Buenas tardes, señor")
    else:
        decir("Buenas noches, señor")

def comando():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Escuchando...")
        r.pause_threshold = 1
        audio = r.listen(source)
    
    try:
        print("Reconociendo...")
        query = r.recognize_google(audio, language='es-es')
        query = re.sub(r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", unicodedata.normalize( "NFD", query), 0, re.I)
        query = unicodedata.normalize("NFC", query)
        print(f"Has dicho: {query}\n")
    except Exception as e:
        print(e)
        print("No te entiendo")
        decir("No te entiendo, por favor repitemelo")
        return None
    
    return query

if __name__ == "__main__":
    saluda()
    decir("Soy Jarvis, por favor, dime como puedo ayudarte")
    while not(salir):
        query = comando()
        if query != None:
            query = query.lower()
            if "wikipedia" in query:
                aceptar()
                decir("Buscando en la wikipedia...")
                query = query.replace("wikipedia", "")
                result = wikipedia.summary(query, sentences=2)
                decir("Segun Wikipedia")
                decir(result)
                print(result)
            elif ("abre youtube" in query) or ("abrir youtube" in query):
                aceptar()
                webbrowser.open("youtube.com")
            elif ("abre google" in query) or ("abrir google" in query):
                aceptar()
                webbrowser.open("google.com")
            elif ("abre stackoverflow" in query) or ("abrir stackoverflow" in query):
                aceptar()
                webbrowser.open("stackoverflow.com")
            elif ("abre sublime text" in query) or ("abrir sublime text" in query):
                aceptar()
                os.startfile("C:\\Program Files\\Sublime Text 3\\sublime_text.exe")
            elif "donde abres" in query:
                print(webbrowser.get())
            elif "pon musica" in query:
                aceptar()
                directorioMusica = "C:\\Users\\Diego Martin\\Music\\"
                canciones = os.listdir(directorioMusica)
                # print(canciones)
                os.startfile(os.path.join(directorioMusica, random.choice(canciones)))
            elif (" hora " in query) and (not("wikipedia") in query):
                aceptar()
                hora = datetime.datetime.now().strftime("%H:%M")
                decir(f"Son las {hora}")
            elif "manda un correo" in query:
                aceptar()
                try:
                    decir("Qué digo?")
                    contenido = comando()
                    decir("bien")
                    decir("Escribe a quien se lo mando")
                    persona = input("Introduce correo: ")
                    print(persona)
                    sendEmail(persona, contenido)
                    decir("correo enviado")
                except Exception as e:
                    print(e)
                    decir("Tu correo no ha sido enviado")
            elif "consola" in query:
                aceptar()
                pyautogui.keyDown("win")
                pyautogui.press("d")
                pyautogui.keyUp("win")
                pyautogui.click()
                try:
                    img = pyautogui.locateOnScreen("./jarvis_del_tio_de_youtube/necesario/linux.PNG")
                    coord = pyautogui.center(img)
                    sitio = list(coord)
                    pyautogui.moveTo(sitio[0], sitio[1], duration=2)
                    pyautogui.doubleClick()
                except Exception as e:
                    decir("Lo siento, no he podido localizarlo en el escritorio")
                    decir("Lo intentare en la barra de busqueda")
                    try:
                        pyautogui.press("win")
                        pyautogui.typewrite("Git Bas", interval=0.2)
                        img2 = pyautogui.locateOnScreen("./jarvis_del_tio_de_youtube/necesario/linux2.png")
                        coord2 = pyautogui.center(img2)
                        sitio2 = list(coord2)
                        pyautogui.moveTo(sitio2[0], sitio2[1], duration=1.5)
                        pyautogui.doubleClick()
                    except Exception as e2:
                        print(e2)
            elif "quien eres" in query:
                decir("Soy Jarvis, la esclava de Diego, estoy aqui para hacerle los deberes, aunque eso esta por ver")
            elif "cuentame un chiste" in query:
                chiste()
            elif "whatsapp" in query and ("me" in query or "mi" in query):
                de = "whatsapp:+14155238886"
                # decir("Desde que numero se envia?")
                # de = obtenerTelefono()
                # decir("Para que numero es el mensaje?")
                # para = obtenerTelefono()
                para = "whatsapp:+34[Numero de telefono]"
                decir("Dictame el mensaje, por favor")
                men = comando()
                enviameWhas(de=de, para=para, mensaje=men)
            elif "whatsapp" in query and not("me" in query or "mi" in query):
                decir("Abre el escaner de Whatsapp Web en tu movil, por favor")
                os.startfile("C:/Program Files (x86)/Google/Chrome/Application/chrome.exe")
                time.sleep(0.5)
                pyautogui.typewrite("http://web.whatsapp.com", interval=0.1)
                pyautogui.press("enter")
                decir("Tienes cinco segundos para escanear el codigo")
                time.sleep(5.0)
                try:
                    pyautogui.locateOnScreen("./jarvis_del_tio_de_youtube/necesario/estasEnWasap.PNG")
                    dentroDeWasap = True
                except Exception as e:
                    decir("Muy lento vaquero, preparate y vuelve a intentarlo")
                    pyautogui.moveTo(x=1910, y=5, duration=2.0)
                    time.sleep(0.2)
                    pyautogui.click()
                    dentroDeWasap = False

                if dentroDeWasap:
                    decir("Tienes diez segundos para seleccionar la conversacion, despues dictame lo que debo escribir")
                    time.sleep(10.0)
                    try:
                        coords = list(pyautogui.center(pyautogui.locateOnScreen("./jarvis_del_tio_de_youtube/necesario/inputWasap.PNG")))
                        pyautogui.moveTo(x=coords[0], y=coords[1], duration=1.0)
                        pyautogui.click()
                        inputWEncontrado = True
                    except Exception as e:
                        print("No encuentro el input")
                        inputWEncontrado = False
                    
                    if inputWEncontrado:
                        escribirMensaje()
            elif "que puedes hacer" in query:
                decir("Hago busquedas en Wikipedia, abro Youtube, Google o Stackoverflow, puedo poner música de tu ordenador, te digo la hora, puedo mandar correos a quien quieras, te abro la consola de Linux, te digo quien soy, te cuento chistes, y te puedo enviar guatsaps")
            elif "horario" in query:
                decir("De que dia quieres saber?")
                dia = comando()
                decir("Que hora quieres saber?")
                hora = comando()
                if dia != None and hora != None:
                    # con = Conexion.ConexionMySQL()
                    hora = int(letraANumero(hora))
                    print(dia)
                    print(hora)
                    if hora < 7:
                        # hora = str(letraANumero(hora))
                        try:
                            info = ConBBDD.sacaInfo(hora, dia)[0]
                            decir("A esa hora los " + dia + " tienes " + info)
                        except Exception as e:
                            decir("Algo ha ido mal")
                else:
                    decir("No he entendido el dia o la hora")
            elif "tiempo hace" in query:
                datosLugar()
            elif salir_en_query(query):
                break
decir("Hasta luego")
saluda()
