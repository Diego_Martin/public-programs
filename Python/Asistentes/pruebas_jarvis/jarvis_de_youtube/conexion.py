import pymysql
class Conexion(object):
    def __init__(self):
        self.ConexionMySQL()

    def ConexionMySQL(self):
        res = None
        try:
            con = pymysql.connect(
                host = "localhost",
                user = "root",
                passwd = "",
                db = "Horario"
            )
            print("Conexion a la base de datos realizada con exito")
            res = con
        except Exception as e:
            print(e)
            res = None
        
        return res

    def sacaInfo(self, hora, dia):
        con = self.ConexionMySQL()
        cursor = con.cursor()
        cursor.execute("SELECT `" + dia + "` FROM `horario` WHERE `hora` = " + str(hora))
        for fila in cursor:
            return fila