import requests
from bs4 import BeautifulSoup

URL = "https://www.amazon.es/ESR-Protector-Pantalla-Templado-Definicion/dp/B01MZ8OZ31/ref=sr_1_1_sspa?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=tablet+samsung&qid=1570972027&sr=8-1-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFDWEVXNFc1RkhYNUImZW5jcnlwdGVkSWQ9QTA3NTU1MzQzUE9RN0c3VlZFMDcyJmVuY3J5cHRlZEFkSWQ9QTA1NzgyODgxNkRXSTcxNEk0VE5VJndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ=="

headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"}

pagina = requests.get(URL, headers=headers)

soup = BeautifulSoup(pagina.content, "html.parser")

print(soup.prettify())
# title = soup.find(id="productTitle").get_text()

# print(title)