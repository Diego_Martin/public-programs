from selenium import webdriver
import time, platform, json

class PideJustEat():
	def __init__(self, calle, numero):
		self.calle = calle
		self.numero = numero
		self.SO = platform.system()
		with open("./utiles/config.json", "r", encoding="utf-8") as conf:
			self.config = json.load(conf)
			conf.close()
		if self.SO == "Windows":
			if "chrome" in self.config["datos"]["navegador"]:
				self.driver = webdriver.Chrome(executable_path="./utiles/chromedriver.exe")
			if "firefox" in self.config["datos"]["navegador"]:
				self.driver = webdriver.Firefox(executable_path="./utiles/geckodriver.exe")
		if self.SO == "Linux":
			if "firefox" in self.config["datos"]["navegador"]:
				self.driver = webdriver.Firefox(executable_path="./utiles/geckodriver")
			else:
				self.hablar("Esta opcion no la tengo programada")
				self.driver = None
	
	def entrarEnLaWeb(self):
		if self.driver:
			self.driver.get("https://www.just-eat.es/")
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-fullAddress-input")
			buscadorAddr.send_keys("Calle Santo Domingo Savio, Madrid, España")
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-actionBtn")
			for _ in range(3):
				buscadorAddr.click()
			time.sleep(3)
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-streetNumber-input")
			buscadorAddr.send_keys("2")
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-actionBtn")
			for _ in range(3):
				buscadorAddr.click()
			time.sleep(3)
			buscadorAddr = self.driver.find_element_by_class_name("c-searchInput")
			buscadorAddr.send_keys("pizza")

pedido = PideJustEat(calle="Calle Santo Domingo Savio, Madrid, España", numero="2")
pedido.entrarEnLaWeb()