import speech_recognition as sr
import unicodedata
import re

def acent(texto):
    texto = re.sub(r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", unicodedata.normalize("NFD", texto), 0, re.I)
    texto = unicodedata.normalize('NFC', texto)
    return texto

def escuchar():
    reconocedor = sr.Recognizer()
    with sr.Microphone() as sonido:
        print("Escuchando...")
        reconocedor.adjust_for_ambient_noise(sonido)
        audio = reconocedor.listen(sonido)
        orden = ""
        try:
            print("Reconociendo...")
            orden = reconocedor.recognize_google(audio, language="es-es")
            orden = acent(orden)
            print(f"Has dicho: {orden.lower()}.\n")
        except Exception as e:
            print("Error al reconocer audio: " + str(e))
    
    return orden.lower()

a = escuchar()