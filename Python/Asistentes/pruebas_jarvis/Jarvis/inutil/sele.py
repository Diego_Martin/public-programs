from selenium import webdriver
from selenium.webdriver.common.by import By
import os, time

driver = webdriver.Chrome(executable_path=f'{os.getcwd()}\\utiles\\chromedriver.exe')

driver.get('https://www.just-eat.es/')

buscadorAddr = driver.find_element_by_class_name("addressLookup-fullAddress-input")
buscadorAddr.send_keys("Calle Santo Domingo Savio, Madrid, España")
buscadorAddr = driver.find_element_by_class_name("addressLookup-actionBtn")
for _ in range(3):
    buscadorAddr.click()
time.sleep(1)
buscadorAddr = driver.find_element_by_class_name("addressLookup-streetNumber-input")
buscadorAddr.send_keys("2")
buscadorAddr = driver.find_element_by_class_name("addressLookup-actionBtn")
for _ in range(3):
    buscadorAddr.click()
time.sleep(2)
buscadorAddr = driver.find_element_by_class_name("c-searchInput")
buscadorAddr.send_keys("pizza")