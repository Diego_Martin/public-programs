import datetime
import getpass
import json
import mss
import os
import platform
import playsound
import poplib
import pyautogui
import pygame
import pyttsx3
import random
import re
from selenium import webdriver
import smtplib
import speech_recognition as sr
import socket
import subprocess
import sys
import time
import threading
import tkinter
import tkinter.filedialog
from twilio.rest import Client as WClient
import unicodedata
import webbrowser
import wikipedia

class Asistente():
	class defineParametros():
		def __init__(self, nombre, user):
			self.nombreAsis = nombre
			self.usuario = user
			with open("./utiles/config.json", "r", encoding="utf-8") as conf:
				self.info = json.load(conf)
				conf.close()
			self.tieneSublimeText = False
			self.SO = platform.system()
			if self.info["datos"]["sublime"] == "" or self.info["datos"]["sublime"] == None:
				if self.SO == "Windows":
					self.cmd = [r"where", r"-R", r"C:\Program Files", r"sublime_text.exe"]
				if self.SO == "Linux":
					self.cmd = ["whereis sublime-text"]
				try:
					res = subprocess.check_output(self.cmd, shell=True)
					if "sublime" in res:
						if self.SO == "Windows":
							res = res.replace("\\", "/")
							self.ruta = f"'{res}' %s"
						if self.SO == "Linux":
							self.ruta = res[14:-2]
				except Exception as e:
					print(f"No se ha podido configurar automaticamente. Error: {e}")
					self.ejecutar()
		
		def ejecutar(self):
			self.ven = tkinter.Tk()
			# Ventana
			self.ven.resizable(False, False)
			self.ven.title(f"Configuracion de {self.nombreAsis}.")
			self.ven.geometry("800x300")
			# Elementos de la ventana
			# Configuracion de la musica
			self.rutaMusica = self.info["datos"]["musica"]
			self.textoRutaMusica = tkinter.Label(self.ven, text=f"Ruta de la musica: {self.rutaMusica}")
			self.textoRutaMusica.place(x=0, y=0)
			tkinter.Button(self.ven, text="Examinar...", command=self.seleCarpetaMusica).place(x=0, y=35)
			# Configuracion del navegador
			self.rutaNav = self.info["datos"]["navegador"]
			self.textoRutaNav = tkinter.Label(self.ven, text=f"Ruta del navegador: {self.rutaNav}")
			self.textoRutaNav.place(x=0, y=80)
			tkinter.Button(self.ven, text="Examinar...", command=self.seleArchivoNav).place(x=0, y=115)
			# Configuracion del sublime text
			self.rutaSubl = self.info["datos"]["sublime"]
			self.textoRutaSubl = tkinter.Label(self.ven, text=f"Ruta del Sublime Text: {self.rutaSubl}")
			self.textoRutaSubl.place(x=0, y=150)
			tkinter.Button(self.ven, text="Examinar...", command=self.seleArchivoSubl).place(x=0, y=185)
			# Muestra la ventana
			self.ven.mainloop()
			self.guarda()

		def seleCarpetaMusica(self):
			self.info["datos"]["musica"] = tkinter.filedialog.askdirectory() + "/"
			self.rutaMusica = self.info["datos"]["musica"]
			self.textoRutaMusica.configure(text=f"Ruta de la musica: {self.rutaMusica}")
			self.guarda()
			return True

		def seleArchivoNav(self):
			self.info["datos"]["navegador"] = tkinter.filedialog.askopenfile().name
			self.rutaNav = self.info["datos"]["navegador"]
			self.textoRutaNav.configure(text=f"Ruta del navegador: {self.rutaNav[0:-4]}")
			self.info["datos"]["navegador"] = f"'{self.rutaNav}' %s"
			self.guarda()
			return True

		def seleArchivoSubl(self):
			self.info["datos"]["sublime"] = tkinter.filedialog.askopenfile().name
			self.rutaSubl = self.info["datos"]["sublime"]
			self.textoRutaSubl.configure(text=f"Ruta del Sublime Text: {self.rutaSubl[0:-4]}")
			self.info["datos"]["sublime"] = f"'{self.rutaSubl}' %s"
			self.guarda()
			return True

		def guarda(self):
			with open("./utiles/config.json", "w", encoding="utf-8") as conf:
				json.dump(self.info, conf, indent=4, ensure_ascii=False)
				Asistente.config = self.info
				conf.close()
				
	def __init__(self, nombre):
		pygame.init()
		self.nombre = nombre
		self.salir = False
		self.voz = pyttsx3.init("sapi5")
		self.voz.setProperty('voices', 'spanish')
		self.reconocedor = sr.Recognizer()
		self.mesesAño = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"]
		with open("./utiles/chistes.json", "r", encoding="utf-8") as chi:
			self.chistes = json.load(chi)
			chi.close()
		with open("./utiles/config.json", "r", encoding="utf-8") as conf:
			self.config = json.load(conf)
			conf.close()
		self.argumentos = sys.argv
		self.modoSilencioso = False
		self.modoDictado = False
		self.accion = ""
		self.SO = platform.system()
		if len(self.argumentos) > 1:
			if self.argumentos[1] == "s" or self.argumentos[1] == "-s" or self.argumentos[1] == "/s":
				self.modoSilencioso = True
	
	def comprobarConfig(self):
		for x in self.config["datos"].keys():
			if self.config["datos"][x] == None:
				return True
		
		return False

	def acent(self, texto):
		texto = re.sub(r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", unicodedata.normalize("NFD", texto), 0, re.I)
		texto = unicodedata.normalize('NFC', texto)
		return texto
	
	def hablar(self, texto):
		if not(self.modoSilencioso):
			self.voz.say(texto)
			self.voz.runAndWait()
		else:
			print(texto)
	
	def escuchar(self):
		if not(self.modoSilencioso):
			with sr.Microphone() as sonido:
				print("Escuchando...")
				self.reconocedor.adjust_for_ambient_noise(sonido)
				audio = self.reconocedor.listen(sonido)
				orden = ""
				try:
					print("Reconociendo...")
					orden = self.reconocedor.recognize_google(audio, language="es-es")
					orden = self.acent(orden)
					print(f"Has dicho: {orden.lower()}.\n")
				except Exception as e:
					print("Error al reconocer audio: " + str(e))
			
			return orden.lower()
		else:
			orden = input("Modo silencioso activado, escribe la orden: ")
			return orden.lower()
	
	def aceptar(self):
		self.hablar(random.choice(self.info["data"]["aceptar"]))
	
	def listaEnOrden(self, lista, query, no=[]):
		for sal in lista:
			if len(no) > 0:
				for n in no:
					if (sal in query) and not(n in query):
						return True
			else:
				if sal in query:
					return True

		return False
	
	def registrarUser(self):
		terminaciones = [("o", "a"), ("", "a"), ("e", "a")]
		self.hablar("Por favor, rellena este formulario en consola.")
		sexo = input("Eres hombre o mujer? (v/m): ")
		nsexo = 1 if sexo.lower() == "m" else 0
		nombre = input("Nombre: ")
		apellido = input("Apellido: ")
		contr = getpass.getpass(prompt="Contraseña: ")
		rcontr = getpass.getpass(prompt="Repite la contraseña: ")
		dcumple = int(input("Dia de cumpleaños (En numero, sin indicar el mes): "))
		mcumple = (int(input("Mes de cumpleaños (En numero): ")) - 1)
		acumple = int(input("Año de nacimiento (En numero): "))
		correo = input("Introduce tu correo: ")
		contrcorreo = getpass.getpass(prompt="Contraseña del correo: ")
		rcontrcorreo = getpass.getpass(prompt="Repite la contraseña del correo: ")
		self.hablar("Debes activar el acceso de aplicaciones poco seguras en tu gmail si quieres poder enviar correos con tu voz.")
		webbrowser.get(self.config["datos"]["navegador"]).open("https://myaccount.google.com/lesssecureapps?pli=1")
		if (contr == rcontr) and (contrcorreo == rcontrcorreo):
			datos = {"data": {"nombre":nombre, "apellido":apellido, "contra":contr, "dcumple":dcumple, "mcumple":mcumple, "acumple":acumple, "permisos":1, "ultimoAcceso": datetime.datetime.now().strftime("%d-%m-%Y"), "correo":{"usuario": correo, "pass": contrcorreo}, "ordenesSalir": ["exit ", "adios", "salir", "apagate", "apagar"], "aceptar": [f"vale, señor{terminaciones[1][nsexo]}", f"si, am{terminaciones[0][nsexo]}", f"por supuesto, señor{terminaciones[1][nsexo]}", f"si, señor{terminaciones[1][nsexo]}", f"si, jef{terminaciones[2][nsexo]}"]}}
			with open("./usuarios/" + nombre + ".json", "w", encoding="utf-8") as archivo:
				json.dump(datos, archivo, indent=4, ensure_ascii=False)
				archivo.close()
		else:
			self.hablar("Las contraseñas no son iguales.")
	
	def preguntarQuien(self):
		self.hablar("Hola, debes decirme quien eres para poder usarme")
		self.usuario = input("Escribe tu nombre: ")
		print("./usuarios/" + self.usuario + ".json")
		try:
			with open("./usuarios/" + self.usuario + ".json", "r", encoding="utf-8") as infos:
				self.info = json.load(infos)
				infos.close()

			self.hablar("Escribeme tu contraseña")
			contraseña = getpass.getpass(prompt="Escribela aqui: ")
			if contraseña == self.info["data"]["contra"]:
				self.info["data"]["ultimoAcceso"] = datetime.datetime.now().strftime("%d-%m-%Y")
				with open("./usuarios/" + self.usuario + ".json", "w", encoding="utf-8") as archivo:
					json.dump(self.info, archivo, indent=4, ensure_ascii=False)
					archivo.close()

				return True
			else:
				return False
		except Exception as e:
			print(f"ERROR: {e}")
			self.hablar("Tu no estas en mi base de datos, ¿Quieres registrarte?")
			res = self.escuchar()
			if "si" in res:
				self.registrarUser()
			else:
				return False
	
	def intentaRegistrar(self):
		for N in range(3):
			print(f"Intento de registro: {(N + 1)}")
			i = self.preguntarQuien()
			if i:
				self.venParametros = self.defineParametros(self.nombre, self.usuario)
				if self.comprobarConfig():
					self.hablar("Parece que no estoy configurada del todo, por favor configurame")
					self.definirParametros()
				return True
		
		return False
	
	def saluda(self):
		hora = int(datetime.datetime.now().hour)
		if hora >= 6 and hora < 13:
			self.hablar(f"Buenos dias, {self.usuario}")
		elif hora >= 13 and hora < 19:
			self.hablar(f"Buenas tardes, {self.usuario}")
		else:
			self.hablar(f"Buenas noches, {self.usuario}")
	
	def buscaEnWikipedia(self, frase):
		resultado = ""
		try:
			wikipedia.set_lang("es")
			self.hablar("Buscando en la Wikipedia...")
			sust = ["busca en wikipedia ", "wikipedia", " en wikipedia", "busca en "]
			for cadena in sust:
				frase = frase.replace(cadena, "")
			resultado = wikipedia.summary(frase, sentences=2)
			self.hablar("Segun Wikipedia: ")
		except Exception as e:
			print(f"ERROR: {e}")
			self.hablar("No he encontrado nada sobre eso en la wikipedia")
			resultado = None
		return resultado

	def comprobarCumple(self):
		if ((self.info["data"]["mcumple"] + 1) == datetime.date.today().month) and (self.info["data"]["dcumple"] == datetime.date.today().day):
			self.hablar("Pero antes...                     Quiero desearte un feliz cumpleaños. Que te lo pases muy bien hoy.")
		
		if ((self.info["data"]["mcumple"] + 1) == datetime.date.today().month) and ((self.info["data"]["dcumple"] + 1) == datetime.date.today().day):
			self.hablar("¿Que tal te fue ayer en tu cumpleaños?")
			res = self.escuchar()
			posresb = ["muy bien", "bien"]
			posresm = ["no lo celebre", "no lo he celebrado"]
			for posres in posresb:
				if posres in res:
					self.hablar(random.choice([f"me alegro, {self.usuario}", f"me alegro mucho, {self.usuario}", f"me alegro mucho {self.usuario}, de verdad", f"que guay {self.usuario}"]))
					self.hablar("Hay algo que pueda hacer por ti?")
					return True
			
			for posres in posresm:
				if posres in res:
					self.hablar(random.choice([f"bueno pero...               lo celebraras otro dia, no?", f"no te preocupes {self.usuario}, ya lo celebraras por todo lo alto"]))
					self.hablar("Hay algo que pueda hacer por ti?")
					return True
	
	def email(self, per, cont):
		server = smtplib.SMTP("smtp.gmail.com", 587)
		server.ehlo()
		server.starttls()
		server.login(self.info["data"]["correo"]["usuario"], self.info["data"]["correo"]["pass"])
		server.sendmail(self.info["data"]["correo"]["usuario"], per, cont)
		server.close()
	
	def chiste(self):
		self.hablar(self.chistes[random.randint(0, (len(self.chistes) - 1))])
		playsound.playsound("./media/xd.mp3")

	def captura(self):
		with mss.mss() as pantalla:
			pantalla.shot(output=f"./capturas/captura_{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.png")
			pantalla.close()

	def definirParametros(self):
		self.venParametros.ejecutar()
		with open("./utiles/config.json", "r", encoding="utf-8") as conf:
			self.config = json.load(conf)
			conf.close()
	
	def enviameWhas(self, de="whatsapp:+14155238886", para="whatsapp:+34640382614", mensaje="Mensaje predeterminado"):
		sid = "AC793b69716ab1002a3fb51374a54b3f33"
		token = "fe57ee8f4dfa1e9ab41dd99911a2ff22"
		cliente = WClient(sid, token)
		cliente.messages.create(body=mensaje, from_=de, to=para)
	
	def hacerPlantilla(self, asig, nombre):
		if self.SO == "Windows":
			pyautogui.press("win")
			time.sleep(0.5)
			pyautogui.typewrite("Word", interval=0.3)
			pyautogui.press("enter")
			time.sleep(6.0)
			pyautogui.press("enter")
			pyautogui.moveTo(x=(pyautogui.size()[0] // 2), y=330, duration=1.0)
			pyautogui.doubleClick()
			pyautogui.press("alt")
			pyautogui.typewrite("jhee", interval=0.2)
			for _ in range(1):
				pyautogui.press("down")
			pyautogui.press("enter")
			pyautogui.moveTo(x=557, y=330, duration=0.7)
			pyautogui.click()
			pyautogui.typewrite(datetime.datetime.now().strftime("%d-%m-%Y"), interval=0.1)
			pyautogui.moveTo(x=(pyautogui.size()[0] // 2), y=330, duration=0.7)
			pyautogui.click()
			pyautogui.typewrite(asig, interval=0.1)
			pyautogui.moveTo(x=1273, y=330, duration=0.7)
			pyautogui.click()
			pyautogui.typewrite(nombre, interval=0.1)
			pyautogui.moveTo(x=936, y=335, duration=1.5)
			pyautogui.tripleClick()
			pyautogui.keyDown("ctrl")
			pyautogui.press("s")
			pyautogui.keyUp("ctrl")
			pyautogui.press("alt")
			pyautogui.typewrite("o3")
			for _ in range(1):
				pyautogui.press("down")
			pyautogui.press("enter")
			pyautogui.press("alt")
			pyautogui.typewrite("o3s")
			for _ in range(1):
				pyautogui.press("down")
			for _ in range(0):
				pyautogui.press("left")
			pyautogui.press("enter")
			pyautogui.press("down")
			pyautogui.scroll(-1500)
			pyautogui.moveTo(x=(pyautogui.size()[0] // 2),y=915,duration=0.7)
			pyautogui.press("alt")
			time.sleep(0.5)
			pyautogui.typewrite("jhip", interval=0.3)
			for _ in range(9):
				pyautogui.press("down")
			pyautogui.press("enter")
			pyautogui.moveTo(x=600, y=885, duration=0.7)
			pyautogui.doubleClick()
			pyautogui.press("backspace")
			pyautogui.press("alt")
			pyautogui.typewrite("odoa", interval=0.2)
			pyautogui.press("enter")
			pyautogui.typewrite(nombre, interval=0.2)
			pyautogui.moveTo(x=1268, y=880, duration=0.7)
			pyautogui.click()
			pyautogui.moveTo(x=1268, y=860, duration=0.7)
			pyautogui.click()
			pyautogui.press("delete")
			pyautogui.moveTo(x=1270, y=880, duration=0.7)
			pyautogui.doubleClick()
			pyautogui.press("alt")
			pyautogui.typewrite("belc", interval=0.2)
			time.sleep(0.4)
			for _ in range(48):
				pyautogui.press("down")
				time.sleep(0.1)
			pyautogui.press("enter")
			pyautogui.press("alt")
			pyautogui.typewrite("bzpoo", interval=0.2)
			for _ in range(5):
				pyautogui.press("down")
			pyautogui.press("enter")
			pyautogui.moveTo(x=(pyautogui.size()[0] // 2), y=(pyautogui.size()[1] // 2), duration=0.7)
			pyautogui.click()
			pyautogui.scroll(3500)
			pyautogui.moveTo(x=712, y=570, duration=0.5)
			pyautogui.doubleClick()
			pyautogui.typewrite(datetime.datetime.now().strftime("%d-%m-%Y"), interval=0.1)
			pyautogui.moveTo(x=1015, y=567, duration=0.3)
			pyautogui.tripleClick()
			time.sleep(0.4)
			pyautogui.typewrite(asig, interval=0.1)
			pyautogui.press("tab")
			pyautogui.typewrite(nombre, interval=0.1)
		else:
			print("La opcion para tu sistema operativo aun no esta programada")
	
	def obtenerSeleccion(self):
		pyautogui.hotkey("ctrl", "c")
		return tkinter.Tk().clipboard_get()

	def mensajesSinLeer(self):
		try:
			servidor = poplib.POP3_SSL("pop.gmail.com")
			servidor.user(self.info["data"]["correo"]["usuario"])
			servidor.pass_(self.info["data"]["correo"]["pass"])
			return len(servidor.list()[1])
		except poplib.error_proto as e:
			webbrowser.get(self.config["datos"]["navegador"]).open("https://myaccount.google.com/lesssecureapps?pli=1")
			self.hablar("tienes que habilitar el acceso a aplicaciones inseguras para que pueda leer tu gmail.")
			print(f"ERROR: {e}")
	
	def ejecutar(self):
		self.intentaRegistrar()
		self.saluda()
		self.hablar(f"Soy {self.nombre}, por favor, dime como puedo ayudarte")
		self.comprobarCumple()
		while not(self.salir):
			orden = self.escuchar()
			if not(self.modoDictado):
				if orden != "":
					self.aceptar()
					if self.listaEnOrden(["hola", f"hola {self.nombre}"], orden, ["busca en youtube", "en youtube"]):
						self.saluda()
					elif "que hora es" in orden:
						hora = datetime.datetime.now().strftime("%H y %M")
						self.hablar(f"Son las {hora}")
					elif "dia es hoy" in orden:
						dia = datetime.date.today()
						self.hablar(f"Hoy es {dia.day} de {self.mesesAño[(dia.month - 1)]}")
						print(dia)
					elif "wikipedia" in orden:
						info = self.buscaEnWikipedia(orden)
						if not(self.modoSilencioso):
							print(info)
						self.hablar(info)
					elif "manda un correo" in orden:
						try:
							self.hablar("¿Qué asunto pongo?")
							asunto = self.escuchar().capitalize()
							self.hablar("¿Qué digo?")
							contenido = self.escuchar().capitalize()
							contenido = f"Subject: {asunto}\n\n{contenido}"
							self.hablar("Bien!")
							self.hablar("Escribe a quien se lo mando")
							persona = input("Introduce correo: ")
							print(persona)
							self.email(persona, contenido)
							self.hablar("Correo enviado con exito")
						except Exception as e:
							print(e)
							self.hablar("Tu correo no ha sido enviado")
					elif self.listaEnOrden(["abre sublime text", "abrir sublime text"], orden):
						os.startfile(self.config["datos"]["sublime"][1:-4])
					elif "pon musica" in orden:
						directorioMusica = self.config["datos"]["musica"]
						canciones = os.listdir(directorioMusica)
						os.startfile(os.path.join(directorioMusica, random.choice(canciones)))
					elif self.listaEnOrden(["cuentame un chiste", "dime algo gracioso"], orden):
						self.chiste()
					elif self.listaEnOrden(["crea un juego en 2d", "crear juego en 2d"], orden):
						self.hablar("Indica la ruta de destino.")
						tk = tkinter.Tk()
						ruta = tkinter.filedialog.askdirectory()
						tk.destroy()
						print(ruta)
						self.hablar("Escribe el nombre del archivo.")
						archivo = input("Sin extension: ")
						print(f"{ruta}/{archivo}.py")
						with open(f"{ruta}/{archivo}.py", "w") as self.a:
							with open("./utiles/hacerpygame.txt", "r", encoding="utf-8") as texto:
								self.a.write(texto.read())
								texto.close()
							self.a.close()
							del self.a
						
						self.hablar("¿Quieres abrirlo?")
						abrir = self.escuchar()
						if "si" in abrir:
							if self.SO == "Windows":
								print(f"""{os.getcwd()}\\utiles\\abrirpygame.bat "{ruta}/{archivo}.py" """)
								os.system(f"""{os.getcwd()}\\utiles\\abrirpygame.bat "{ruta}/{archivo}.py" """)
							elif self.SO == "Linux":
								os.system(f"""{os.getcwd()}\\utiles\\abrirpygame.sh "{ruta}/{archivo}.py" """)
					elif self.listaEnOrden(["abre chrome", "abrir chrome", "abre google chrome", "abrir google chrome"], orden):
						os.startfile(self.config["datos"]["navegador"][1:-4])
					elif self.listaEnOrden(["abre google", "abrir google"], orden):
						webbrowser.get(self.config["datos"]["navegador"]).open("https://www.google.es")
					elif self.listaEnOrden(["abre stackoverflow", "abrir stackoverflow"], orden):
						webbrowser.get(self.config["datos"]["navegador"]).open("https://www.stackoverflow.com")
					elif self.listaEnOrden(["abre youtube", "abrir youtube"], orden):
						webbrowser.get(self.config["datos"]["navegador"]).open("https://www.youtube.com")
					elif ("cual es mi ip" in orden) or ("ipconfig" in orden.replace(" ", "")):
						ip = socket.gethostbyname_ex(socket.gethostname())[2][0]
						print(ip)
						self.hablar(f"Tu direccion ip es la {ip}")
					elif self.listaEnOrden(["haz una captura", "captura la pantalla", "pantalla a imagen"], orden):
						self.captura()
						self.hablar("Hecho, captura guardada en mi directorio de capturas.")
						self.hablar("Quieres abrir el directorio?")
						res = self.escuchar()
						if "si" in res:
							os.startfile(os.path.abspath(os.path.join(os.getcwd(), "./capturas")))
						else:
							self.hablar(f"Vale {self.usuario}")
					elif self.listaEnOrden(["modo ruido", "modo silencioso", "modo silencio", "modo hablado", "modo conversacion", "modo consola"], orden):
						self.modoSilencioso = not(self.modoSilencioso)
					elif self.listaEnOrden(["abre la consola", "abrir consola", "abre consola", "abrir la consola"], orden):
						if self.SO == "Windows":
							pyautogui.hotkey("win", "r")
							time.sleep(0.1)
							pyautogui.typewrite("cmd")
							pyautogui.press("enter")
						if self.SO == "Linux":
							pyautogui.hotkey("ctrl", "alt", "t")
					elif self.listaEnOrden(["definir parametro", "modificar parametro", "modificar ruta", "definir ruta", "cambiar parametros", "cambiar ruta"], orden):
						self.definirParametros()
					elif "whatsapp" in orden and ("me" in orden or "mi" in orden):
						self.hablar("Dictame el mensaje, por favor")
						men = self.escuchar()
						self.enviameWhas(mensaje=men)
					elif self.listaEnOrden(["copia", "copia texto", "copiar texto"], orden):
						if self.SO == "Windows":
							pyautogui.hotkey("ctrl", "c")
						elif self.SO == "Linux":
							pyautogui.hotkey("ctrl", "shift", "c")
					elif self.listaEnOrden(["pega", "pega texto", "pegar texto"], orden):
						if self.SO == "Windows":
							pyautogui.hotkey("ctrl", "v")
						elif self.SO == "Linux":
							pyautogui.hotkey("ctrl", "shift", "v")
					elif self.listaEnOrden(["quien soy", "como me llamo"], orden):
						nombre = self.info["data"]["nombre"]
						apellido = self.info["data"]["apellido"]
						if (datetime.date.today().month - (self.info["data"]["mcumple"] + 1)) > 0:
							edad = datetime.date.today().year - self.info["data"]["acumple"]
						elif (datetime.date.today().month - (self.info["data"]["mcumple"] + 1)) == 0:
							if (datetime.date.today().day - self.info["data"]["dcumple"]) > 0:
								edad = datetime.date.today().year - self.info["data"]["acumple"]
							elif (datetime.date.today().day - self.info["data"]["dcumple"]) > 0:
								self.hablar("Por cierto, hoy es tu cumpleaños")
							else:
								edad = datetime.date.today().year - self.info["data"]["acumple"] - 1
						else:
							edad = datetime.date.today().year - self.info["data"]["acumple"] - 1
						self.hablar(f"Tu eres {nombre} {apellido}, y tienes {edad} años.")
					elif self.listaEnOrden(["cuanto es", "cual es el resultado de"], orden):
						for coso in ["cuanto es", "cual es el resultado de"]:
							orden = orden.replace(coso, "")
						orden = orden.split()
						operandos = [("+", "mas"), ("-", "menos"), ("*", "por"), ("*", "x"), ("/", "entre")]
						for op in operandos:
							if orden[1] in op:
								orden[1] = op[0]
						orden = "".join(orden)
						orden = orden.replace(" ", "")
						print(orden)
						if orden != "0/0":
							try:
								int(orden[0]) + int(orden[len(orden) - 1])
								res = eval(orden)
								self.hablar(f"{orden} es igual a {int(res)}")
							except Exception as e:
								self.hablar("Tienes que operar numeros")
						else:
							self.hablar("Imaginate que quieres dividir 0 novias entre 0, que es la cantidad de veces que has mantenido relaciones. Estas triste porque no tienes novia pero tambien estas triste porque eres como el aceite, virgen extra.")
							playsound.playsound("./media/xd.mp3")
					elif self.listaEnOrden(["busca en youtube", "en youtube"], orden):
						for coso in ["busca en youtube", "en youtube"]:
							orden = orden.replace(coso, "")
						orden = orden.replace(" ", "+")
						url = f"https://www.youtube.com/results?search_query={orden[1:]}"
						print(url)
						webbrowser.get(self.config["datos"]["navegador"]).open(url)
					elif self.listaEnOrden(["modo dictado"], orden):
						self.modoDictado = not(self.modoDictado)
						self.PriEsp = False
					elif self.listaEnOrden(["haz una plantilla", "hacer plantilla", "hacer una plantilla"], orden):
						nombre = self.info["data"]["nombre"].capitalize()
						apellido = self.info["data"]["apellido"].capitalize()
						nombreCompleto = nombre + " " + apellido
						self.hablar("Dime por consola la asignatura de la practica")
						asignatura = self.acent(input("Escribe aqui la asignatura: "))
						self.hacerPlantilla(asignatura, nombreCompleto)
						self.hablar("Hecho")
					elif self.listaEnOrden(["lee en alto", "leer en alto", "lee seleccion", "leer seleccion", "lee la seleccion", "leer la seleccion"], orden):
						self.hablar(self.obtenerSeleccion())
					elif self.listaEnOrden(["pide comida"], orden):
						pedido = PideJustEat(calle="Calle Santo Domingo Savio, Madrid, España", numero="2", nombre=self.nombre)
						pedido.entrarEnLaWeb()
					elif self.listaEnOrden(["cuantos mensajes sin leer"], orden):
						num = self.mensajesSinLeer()
						self.hablar(f"Tienes {num} mensajes sin leer en gemeil")
					elif self.listaEnOrden(self.info["data"]["ordenesSalir"], orden, ["exito"]):
						self.salir = True
					# else:
					# 	self.hablar("Esa orden no esta registrada en mis posibles acciones por el momento.")
			else:
				if orden != "":
					if self.listaEnOrden(["comando salir de modo dictado", "comando salir del modo dictado"], orden):
						self.modoDictado = not(self.modoDictado)
					elif self.listaEnOrden(["comando copiar", "comando copia texto", "comando copiar texto"], orden):
						if self.SO == "Windows":
							pyautogui.hotkey("ctrl", "c")
						elif self.SO == "Linux":
							pyautogui.hotkey("ctrl", "shift", "c")
					elif self.listaEnOrden(["comando pegar", "comando pega texto", "comando pegar texto"], orden):
						if self.SO == "Windows":
							pyautogui.hotkey("ctrl", "v")
						elif self.SO == "Linux":
							pyautogui.hotkey("ctrl", "shift", "v")
					elif self.listaEnOrden(["comando selecciona palabra", "comando seleccionar palabra"], orden):
						pyautogui.hotkey("shift", "ctrl", "left")
					elif self.listaEnOrden(["comando deshacer", "comando retroceder"], orden):
						pyautogui.hotkey("ctrl", "z")
					elif self.listaEnOrden(["comando guardar word"], orden):
						pyautogui.hotkey("ctrl", "g")
					elif self.listaEnOrden(["comando guardar"], orden, ["word"]):
						pyautogui.hotkey("ctrl", "s")
					elif self.listaEnOrden(["comando enter", "comando intro", "comando nueva linea"], orden):
						pyautogui.press("enter")
						self.PriEsp = False
					elif self.listaEnOrden(["comando borrar"], orden):
						pyautogui.press("backspace")
					elif self.listaEnOrden(["comando punto"], orden, ["coma"]):
						pyautogui.typewrite(".")
					elif self.listaEnOrden(["comando coma"], orden, ["punto"]):
						pyautogui.typewrite(",")
					elif self.listaEnOrden(["comando punto y coma"], orden):
						pyautogui.typewrite(";")
					else:
						if self.PriEsp:
							orden = " " + orden
						else:
							orden = orden.replace(orden[0], orden[0].upper(), 1)
						pyautogui.typewrite(orden, interval=0.05)
					self.PriEsp = True

		self.hablar("Hasta pronto.")

class PideJustEat(Asistente):
	def __init__(self, calle, numero, nombre):
		self.calle = calle
		self.numero = numero
		self.SO = platform.system()
		super().__init__(nombre=nombre)
		with open("./utiles/config.json", "r", encoding="utf-8") as conf:
			self.config = json.load(conf)
			conf.close()
		if self.SO == "Windows":
			if "chrome" in self.config["datos"]["navegador"]:
				self.driver = webdriver.Chrome(executable_path="./utiles/chromedriver.exe")
			elif "firefox" in self.config["datos"]["navegador"]:
				self.driver = webdriver.Firefox(executable_path="./utiles/geckodriver.exe")
			else:
				super().hablar("Esta opcion no la tengo programada")
		if self.SO == "Linux":
			if "firefox" in self.config["datos"]["navegador"]:
				self.driver = webdriver.Firefox(executable_path="./utiles/geckodriver")
			else:
				super().hablar("Esta opcion no la tengo programada")
				self.driver = None

	def entrarEnLaWeb(self):
		if self.driver:
			self.driver.get("https://www.just-eat.es")
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-fullAddress-input")
			buscadorAddr.send_keys(self.calle)
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-actionBtn")
			for _ in range(3):
				buscadorAddr.click()
			time.sleep(1)
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-streetNumber-input")
			buscadorAddr.send_keys(self.numero)
			buscadorAddr = self.driver.find_element_by_class_name("addressLookup-actionBtn")
			for _ in range(3):
				buscadorAddr.click()
			time.sleep(2)
			buscadorAddr = self.driver.find_element_by_class_name("c-searchInput")
			super().hablar("¿Que comida quieres pedir?")
			comida = super().escuchar()
			buscadorAddr.send_keys(comida)