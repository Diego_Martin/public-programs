#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {False, True} Boolean;

// class Object
// {
// 	public:
// 		virtual const char *toString () = 0;
// 		virtual const char *type () = 0;
// };

class String// : public Object
{
	public:
		const char *toString () {
			return (const char *) valor;
		}

		const char *type () {
			return "String";
		}

		void operator = (const char *frase) {
			this->setValor (frase);
		}

		// String operator + (const char *frase) {
		// 	char *res = (char *) malloc (sizeof (char) * strlen (frase) + strlen (valor));
		// 	strcpy (res, valor);
		// 	strcat (res, frase);
		// 	String result;
		// 	result.setValor((const char *) res);
		// 	free (res);
		// 	return result;
		// }

		// void operator += (const char *frase) {
		// 	char *res = (char *) malloc (sizeof (char) * strlen (frase) + strlen (this->valor));
		// 	strcpy (res, this->valor);
		// 	strcat (res, frase);
		// 	this->setValor ((const char *) res);
		// 	free (res);
		// }

		// String operator[] (int index) { // Por terminar
		// 	char *frase = (char *) malloc (sizeof (char));
		// 	sprintf (frase, "%s", (this->valor) + index);
		// 	// printf("%s", frase);
		// 	String result;
		// 	result.setValor((const char *) frase);
		// 	return result;
		// }

		void setValor (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
		}
		~String () {
			// printf("Se cierra el programa\n");
			// free (this->car);
			// free (this->valor);
			delete valor;
			delete car;
		}
	private:
		char *car;
		char *valor;
};

template <class Tipo>
String type (Tipo var) {
	char *frase = (char *) malloc (sizeof (char) * (strlen (var.type()) + 10));
	sprintf (frase, "<class '%s'>", var.type ());
	String res;
	res.setValor ((const char *) frase);
	return res;
}

template <class Tipo>
void print (Tipo texto, const char *fin = "\n") { // Por aqui debe haber algun error
	printf ("%s%s", texto.toString(), fin);
}


int main(int argc, char const *argv[])
{
	String hola;
	hola = "Hola que tal";
	print (hola);
	return 0;
}
