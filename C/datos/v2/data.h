#ifndef __DATA_H__
#define __DATA_H__


// Clase abstracta por tener metodos virtuales
class Object
{
	public:
		virtual const char *toString () {}
		virtual const char *type () {}
};

class String : public Object
{
	public:
		const char *toString () {
			return (const char *) this->valor;
		}

		const char *type () {
			return "String";
		}
	private:
		char *valor;
};


#endif