#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <typeinfo>


class Object
{
	public:
		// Object();
		// ~Object();
		virtual const char *toString () = 0;
		virtual const char *type () = 0;
		virtual void setValor (const char *frase) = 0;
};

class String : public Object
{
	public:
		String () {}
		String (const char *texto) {
			this->setValor (texto);
		}
		String (char *texto) {
			this->setValor ((const char *) texto);
		}
		String (char texto) {
			char *res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->setValor ((const char *) res);
		}

		const char *toString () {
			return (const char *) this->valor;
		}

		const char *type () {
			return "String";
		}

		char operator [] (int index) {
			if (index > -1) {
				if (index < this->length()) {
					char res = *(this->valor + index);
					return res;
				} else {
					printf("\033[31;40mEl indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->length())) {
					char res = *(this->valor + this->length() + index);
					return res;
				} else {
					printf("\033[31;40mEl indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

		void operator = (const char *frase) {
			this->setValor (frase);
		}

		void operator = (String frase) {
			this->setValor (frase.toString());
		}

		int length () {
			return strlen (this->valor);
		}

		void setValor (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
			// free (this->valor);
		}

		String operator + (const char *frase) {
			String res;
			char *resultado = (char *) malloc (sizeof (char) * (strlen (this->valor) + strlen (frase)));
			strcpy (resultado, this->valor);
			strcat (resultado, frase);
			res = (const char *) resultado;
			return res;
		}

		String operator + (String frase) {
			String res;
			char *resultado = (char *) malloc (sizeof (char) * (strlen (this->valor) + strlen (frase.toString ())));
			strcpy (resultado, this->valor);
			strcat (resultado, frase.toString ());
			res = (const char *) resultado;
			return res;
		}

		bool operator == (const char *frase) {
			return !((bool) strcmp ((const char *) this->valor, frase));
		}

		bool operator == (String frase) {
			return !((bool) strcmp ((const char *) this->valor, frase.toString ()));
		}

		~String () {
			delete this->valor;
		}
	private:
		char *valor;
};

class Boolean : public Object
{
	public:
		Boolean () {}
		Boolean (bool valor) {
			this->valor = valor;
		}
		Boolean (int valor) {
			this->valor = (bool) valor;
		}

		const char *toString () {
			return ((this->valor) ? "True" : "False");
		}

		const char *type () {
			return "Boolean";
		}
		void setValor (const char *frase) {return;}
		
		bool getValor () {
			return this->valor;
		}

		void operator = (bool valor) {
			this->valor = valor;
		}

		void operator = (Boolean valor) {
			this->valor = valor.getValor ();
		}

		void operator = (String valor) {
			if (valor == "")
				this->valor = false;
			else
				this->valor = true;
		}

		void operator = (int valor) {
			if (valor == 0)
				this->valor = false;
			else
				this->valor = true;
		}

		bool operator ! () {
			return !this->valor;
		}
	private:
		bool valor;
};

class Integer : public Object
{
	public:
		Integer () {}
		Integer (int num) {
			this->valor = num;
		}
		Integer (float num) {
			this->valor = (int) num;
		}
		Integer (double num) {
			this->valor = (int) num;
		}

		const char *toString () {
			int contador = 1;
			long long int aux = this->valor;
			
			if (aux >= 0) {
				while(aux / 10 > 0) {
					aux /= 10;
					contador++;
				}
				this->res = (char *) malloc (sizeof (char) * contador);
			} else {
				while(aux * 10 > 0) {
					aux *= 10;
					contador++;
				}
				this->res = (char *) malloc (sizeof (char) * (contador + 1));
			}
			sprintf (this->res, "%lli", this->valor);
			// free (res); // Da error
			return (const char *) res;
		}

		const char *type () {
			return "Integer";
		}
		void setValor (const char *frase) {return;}
		
		void operator = (int valor) {
			this->valor = valor;
		}

		void operator = (Integer valor) {
			this->valor = valor.getValor ();
		}

		Integer operator + (int valor) {
			Integer res;
			valor += this->valor;
			res = valor;
			return res;
		}

		Integer operator + (Integer valor) {
			Integer res;
			int acumulador = this->valor + valor.getValor();
			res = acumulador;
			return res;
		}

		void operator += (int valor) {
			this->valor += valor;
		}

		void operator += (Integer valor) {
			this->valor += valor.getValor();
		}

		Integer operator - (int valor) {
			Integer res;
			valor -= this->valor;
			res = valor;
			return res;
		}

		Integer operator - (Integer valor) {
			Integer res;
			int acumulador = this->valor - valor.getValor();
			res = acumulador;
			return res;
		}

		void operator -= (int valor) {
			this->valor -= valor;
		}

		void operator -= (Integer valor) {
			this->valor -= valor.getValor();
		}

		Integer operator * (int valor) {
			Integer res;
			valor *= this->valor;
			res = valor;
			return res;
		}

		Integer operator * (Integer valor) {
			Integer res;
			int acumulador = this->valor * valor.getValor();
			res = acumulador;
			return res;
		}

		void operator *= (int valor) {
			this->valor *= valor;
		}

		void operator *= (Integer valor) {
			this->valor *= valor.getValor();
		}

		Integer operator / (int valor) {
			Integer res;
			valor /= this->valor;
			res = valor;
			return res;
		}

		Integer operator / (Integer valor) {
			Integer res;
			int acumulador = this->valor / valor.getValor();
			res = acumulador;
			return res;
		}
		
		void operator /= (int valor) {
			this->valor /= valor;
		}

		void operator /= (Integer valor) {
			this->valor /= valor.getValor();
		}

		Integer operator % (int valor) {
			Integer res;
			valor %= this->valor;
			res = valor;
			return res;
		}

		Integer operator % (Integer valor) {
			Integer res;
			int acumulador = this->valor % valor.getValor();
			res = acumulador;
			return res;
		}

		void operator %= (int valor) {
			this->valor %= valor;
		}

		void operator %= (Integer valor) {
			this->valor %= valor.getValor();
		}

		bool operator == (int valor) {
			return (this->valor == valor);
		}

		bool operator == (Integer valor) {
			return (this->valor == valor.getValor());
		}

		void operator ++ () {
			this->valor++;
		}

		void operator -- () {
			this->valor--;
		}

		bool operator < (int valor) {
			return (this->valor < valor);
		}

		bool operator < (Integer valor) {
			return (this->valor < valor.getValor ());
		}

		bool operator > (int valor) {
			return (this->valor > valor);
		}

		bool operator > (Integer valor) {
			return (this->valor > valor.getValor());
		}

		bool operator <= (int valor) {
			return (this->valor <= valor);
		}

		bool operator <= (Integer valor) {
			return (this->valor <= valor.getValor ());
		}

		bool operator >= (int valor) {
			return (this->valor >= valor);
		}

		bool operator >= (Integer valor) {
			return (this->valor >= valor.getValor());
		}

		Integer pow (int exp) {
			Integer res;
			res = 1;
			if (exp == 0 && this->valor == 0) {
				printf("\033[31;40mTanto la base como el exponente deben ser distintos de 0.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
			if (!exp)
				return res;

			for (int i = 0; i < exp; i++)
				res = res * this->valor;

			return res;
		}

		Integer pow (Integer exp) {
			return this->pow(exp.getValor());
		}

		Integer sqrtd () {
			Integer res;
			res = sqrt (this->valor);
			return res;
		}

		static long long int max () {
			return LLONG_MAX;
		}

		static long long int min () {
			return LLONG_MIN;
		}

		long long int getValor () {
			return this->valor;
		}

		static Integer parseInt (const char *frase) {
			bool esEntero = true;
			for (int i = 0; i < strlen (frase); i++)
				if (!(*(frase + i) < ':' && *(frase + i) > '/'))
					esEntero = false;

			if (esEntero) {
				Integer res;
				res = atoi (frase);
				return res;
			} else {
				printf ("\033[31;40mEl parámetro pasado a Integer::parseInt() contiene caracteres no numericos.\n\033[97;40m]]");
				exit (EXIT_FAILURE);
			}
		}

		static Integer parseInt (String frase) {
			return Integer::parseInt(frase.toString ());
		}
	private:
		char *res;
		long long int valor;
};

class Float : public Object
{
	public:
		Float () {}
		Float (int num) {
			this->valor = (double) num;
		}
		Float (float num) {
			this->valor = (double) num;
		}
		Float (double num) {
			this->valor = num;
		}
		const char *toString () {
			char *res = (char *) malloc (sizeof (char) * 35);
			sprintf (res, "%lf", this->valor);
			return (const char *) res;
		}
		const char *type () {
			return "Float";
		}
		void setValor (const char *frase) {return;}
	private:
		double valor;
};

class Array : public Object
{
	public:
		Array () {
			this->len = 0;
			this->array = (Object **) malloc (sizeof (Object *) * this->len);
		}
		const char *toString () {
			String resultado;
			resultado = "[";
			printf ("%s", resultado.toString ());
			for (int i = 0; i < this->len; i++) {
				resultado = resultado + (*this->array + i)->toString ();
				printf ("%s", resultado.toString ());
			}
			resultado = resultado + "]";
			printf ("%s", resultado.toString ());
			return resultado.toString ();
		}

		const char *type () {
			return "Array";
		}
		void setValor (const char *frase) {return;}
		
		void append (Object *item) {
			this->len++;
			array = (Object **) realloc (array, sizeof (Object *) * this->len);
			*(this->array + this->len - 1) = item;
		}

		int length () {
			return this->len;
		}

		Object *operator [] (int index) {
			if (index < this->len && index > -1)
				return *(array + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(array + (-1 * index));
			else {
				printf ("\033[31;40mEl indice debe ser menor que la longitud de la lista y mayor que la longitud de la lista en negativo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
		}
	private:
		Object **array;
		int len;
};

Object *Concat (Object *cosa1, Object *cosa2) {
	Object *temp(NULL);
	temp = new String;
	char *resultado = (char *) malloc (sizeof (char) * (strlen (cosa1->toString ()) + strlen (cosa2->toString ())));
	strcpy (resultado, cosa1->toString());
	strcat (resultado, cosa2->toString());
	temp->setValor((const char *) resultado);
	return temp;
}

// ------------------- print ---------------------
void print (Object *texto, const char *fin = "\n") {
	printf("%s%s", texto->toString(), fin);
}

void print (const char *texto, const char *fin = "\n") {
	printf("%s%s", texto, fin);
}

void print (char *texto, const char *fin = "\n") {
	printf("%s%s", texto, fin);
}

void print (char texto, const char *fin = "\n") {
	printf("%c%s", texto, fin);
}

void print (int texto, const char *fin = "\n") {
	printf("%i%s", texto, fin);
}

void print (short int texto, const char *fin = "\n") {
	printf("%hi%s", texto, fin);
}

void print (long int texto, const char *fin = "\n") {
	printf("%li%s", texto, fin);
}

void print (long long int texto, const char *fin = "\n") {
	printf("%lli%s", texto, fin);
}

void print (unsigned texto, const char *fin = "\n") {
	printf("%u%s", texto, fin);
}

void print (unsigned short texto, const char *fin = "\n") {
	printf("%hu%s", texto, fin);
}

void print (long unsigned texto, const char *fin = "\n") {
	printf("%lu%s", texto, fin);
}

void print (long long unsigned texto, const char *fin = "\n") {
	printf("%llu%s", texto, fin);
}

void print (float texto, const char *fin = "\n") {
	printf("%f%s", texto, fin);
}

void print (double texto, const char *fin = "\n") {
	printf("%lf%s", texto, fin);
}

void print (bool texto, const char *fin = "\n") {
	printf("%s%s", (texto) ? "True" : "False", fin);
}

// ------------------- type (Object) ----------------------
Object *type (Object *var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * (strlen (var->type()) + 10));
	sprintf (res, "<class '%s'>", var->type());
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (const char *) ----------------------
Object *type (const char *var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 22);
	sprintf (res, "<class 'const char *'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (char *) ----------------------
Object *type (char *var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 16);
	sprintf (res, "<class 'char *'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (char) ----------------------
Object *type (char var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 14);
	sprintf (res, "<class 'char'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (int) ----------------------
Object *type (int var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 13);
	sprintf (res, "<class 'int'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (short) ----------------------
Object *type (short var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 15);
	sprintf (res, "<class 'short'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long int) ----------------------
Object *type (long int var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 18);
	sprintf (res, "<class 'long int'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long long int) ----------------------
Object *type (long long int var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 23);
	sprintf (res, "<class 'long long int'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (unsigned) ----------------------
Object *type (unsigned var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 18);
	sprintf (res, "<class 'unsigned'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long unsigned) ----------------------
Object *type (long unsigned var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 23);
	sprintf (res, "<class 'long unsigned'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (long long unsigned) ----------------------
Object *type (long long unsigned var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 28);
	sprintf (res, "<class 'long long unsigned'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (float) ----------------------
Object *type (float var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 15);
	sprintf (res, "<class 'float'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (double) ----------------------
Object *type (double var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 16);
	sprintf (res, "<class 'double'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

// ------------------- type (bool) ----------------------
Object *type (bool var) {
	Object *temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * 14);
	sprintf (res, "<class 'bool'>");
	temp->setValor((const char *) res);
	free (res);
	return temp;
}


int main(int argc, char const *argv[])
{
	String prueba, prueba2, compara1, compara2;
	Integer entero, entero2, max, parse, potencia; // int
	Boolean booleano; // bool
	Array miArray;
	// String res;
	compara1 = "Diego";
	compara2 = "Diego";
	entero = 2;
	potencia = entero.pow (10);
	entero2 = 73;
	// max = Integer::max ();
	prueba = "Hola";
	prueba2 = "Ke Pacha";
	prueba = prueba2;
	entero = entero2;
	prueba = prueba + " cruc";
	// res = prueba * 3;
	print (&prueba);

	print (Concat (new String ("La potencia de 2 elevado a 10 es "), &potencia));

	print (prueba[-7]);
	// print (&res);
	print ("Adios");

	print (&entero);

	print (Integer::min ());
	// print (&max);

	print (type (&prueba));

	print (type (&entero));

	print (23);

	print (Concat (&entero, Concat(&prueba, &entero2)));

	print ('D', "");

	print (" de Diego");

	print (3.1415);

	print (type (3.4));

	printf ("Longitud de prueba: %i\n", prueba.length());

	parse = Integer::parseInt ("123456");

	print (&parse);

	print ((compara1 == compara2));

	miArray.append (&prueba);
	miArray.append (new Integer(73));
	miArray.append (&booleano);
	// miArray.append (&entero);
	print ("Longitud de miArray: ", "");
	print (miArray.length());
	print (miArray[1]);
	print ("[", "");
	for (int i = 0; i < miArray.length(); i++) {
		print (miArray[i], "");
		print (", ", "");
	}
	printf ("\b\b");
	print ("]");
	print (&miArray);
	return 0;
}
