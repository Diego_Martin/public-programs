#ifndef __DATA_H__
#define __DATA_H__

#include "object.h"
#include "string.h"

// Clase abstracta por tener metodos virtuales
class Object
{
	public:
		// Object();
		const char *toString ();
		const char *type ();

	protected:
		~Object();

	// private:
	
};

class String : public Object
{
	public:
		// String();
		~String();
		const char *toString ();
		const char *type ();
	// protected:
	private:
		char *valor;
};


#endif