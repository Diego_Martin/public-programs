#ifndef __ARRAY_H__
#define __ARRAY_H__

#include <stdio.h>
#include <stdlib.h>
#include "Object.h"
#include "String.h"
#include "Integer.h"


class Array : public Object
{
	public:
		Array () {
			this->len = 0;
			this->array = (Object **) malloc (sizeof (Object *) * this->len);
		}

		// const char *toString () {
		// 	String resultado;
		// 	resultado = "[";
		// 	printf ("Inicio\n");
		// 	for (int i = 0; i < this->len; i++)
		// 		printf ("%s\n", (*this->array + (i * sizeof (Object *)))->toString ());
			
		// 	resultado = resultado + "]";
		// 	printf ("Fin\n");
		// 	return resultado.toString ();
		// }

		const char *toString () {
			int tamano = sizeof (char) * 1;
			char *resultado = (char *) malloc (tamano);
			strcpy (resultado, "[");
			printf ("Inicio\n");
			for (int i = 0; i < this->len; i++) {
				printf ("Tamano: %d\n", tamano);
				tamano += sizeof (char) * strlen ((*this->array + (i * sizeof (Object *)))->toString ());
				printf ("Tamaño sumado.\n");
				resultado = (char *) realloc (resultado, tamano);
				printf ("Realloc ejecutado con exito.\n");
				strcat (resultado, (*this->array + (i * sizeof (Object *)))->toString ());
				printf ("%s\n", resultado);
				if (i < this->len - 1) {
					tamano += 2;
					resultado = (char *) realloc (resultado, tamano);
					strcat (resultado, ", ");
				}
			}
			resultado = (char *) realloc (resultado, tamano + 1);
			strcat (resultado, "]");
			// printf ("Fin\n");
			return (const char *) resultado;
		}

		const char *type () {
			return "Array";
		}
		void setValor (const char *frase) {return;}

		void append (Object *item) {
			this->len++;
			array = (Object **) realloc (array, sizeof (Object *) * this->len);
			*(this->array + this->len - 1) = item;
		}

		int length () {
			return this->len;
		}

		Object *operator [] (int index) {
			if (index < this->len && index > -1)
				return *(array + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(array + (-1 * index));
			else {
				printf ("\033[31;40mEl indice debe ser menor que la longitud de la lista y mayor que la longitud de la lista en negativo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
		}

		Object *operator [] (Integer index) {
			return &this[(int) index.getValor ()];
		}
	private:
		Object **array;
		int len;
};

#endif