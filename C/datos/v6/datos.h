#ifndef __DATOS_H__
#define __DATOS_H__

#include "Object.h"
#include "String.h"
#include "Boolean.h"
#include "Float.h"
#include "Array.h"

#define Obj Object *
#define Function Object *

Function Concat (Obj cosa1, Obj cosa2) {
	Obj temp(NULL);
	temp = new String;
	char *resultado = (char *) malloc (sizeof (char) * (strlen (cosa1->toString ()) + strlen (cosa2->toString ())));
	strcpy (resultado, cosa1->toString());
	strcat (resultado, cosa2->toString());
	temp->setValor((const char *) resultado);
	return temp;
}

void print (Obj texto, const char *fin = "\n") {
	printf("%s%s", texto->toString(), fin);
}

Function type (Obj var) {
	Obj temp(NULL);
	temp = new String;
	char *res = (char *) malloc (sizeof (char) * (strlen (var->type()) + 10));
	sprintf (res, "<class '%s'>", var->type());
	temp->setValor((const char *) res);
	free (res);
	return temp;
}

#endif