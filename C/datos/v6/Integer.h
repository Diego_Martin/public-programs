#ifndef __INTEGER_H__
#define __INTEGER_H__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include "Object.h"
#include "String.h"

class Integer : public Object
{
	public:
		Integer () {}
		Integer (int num) {
			this->valor = num;
		}
		Integer (float num) {
			this->valor = (int) num;
		}
		Integer (double num) {
			this->valor = (int) num;
		}

		const char *toString () {
			int contador = 1;
			long long int aux = this->valor;
			
			if (aux >= 0) {
				while(aux / 10 > 0) {
					aux /= 10;
					contador++;
				}
				this->res = (char *) malloc (sizeof (char) * contador);
			} else {
				while(aux * 10 > 0) {
					aux *= 10;
					contador++;
				}
				this->res = (char *) malloc (sizeof (char) * (contador + 1));
			}
			sprintf (this->res, "%lli", this->valor);
			// free (res); // Da error
			return (const char *) res;
		}

		const char *type () {
			return "Integer";
		}
		void setValor (const char *frase) {return;}

		void operator = (int valor) {
			this->valor = valor;
		}

		void operator = (Integer valor) {
			this->valor = valor.getValor ();
		}

		Integer operator + (int valor) {
			Integer res;
			valor += this->valor;
			res = valor;
			return res;
		}

		Integer operator + (Integer valor) {
			Integer res;
			int acumulador = this->valor + valor.getValor();
			res = acumulador;
			return res;
		}
		
		void operator += (int valor) {
			this->valor += valor;
		}

		void operator += (Integer valor) {
			this->valor += valor.getValor();
		}

		void operator += (Integer *valor) {
			this->valor += valor->getValor();
		}

		Integer operator - (int valor) {
			Integer res;
			valor -= this->valor;
			res = valor;
			return res;
		}

		Integer operator - (Integer valor) {
			Integer res;
			int acumulador = this->valor - valor.getValor();
			res = acumulador;
			return res;
		}

		Integer operator - (Integer *valor) {
			Integer res;
			int acumulador = this->valor - valor->getValor();
			res = acumulador;
			return res;
		}

		void operator -= (int valor) {
			this->valor -= valor;
		}

		void operator -= (Integer valor) {
			this->valor -= valor.getValor();
		}

		Integer operator * (int valor) {
			Integer res;
			valor *= this->valor;
			res = valor;
			return res;
		}

		Integer operator * (Integer valor) {
			Integer res;
			int acumulador = this->valor * valor.getValor();
			res = acumulador;
			return res;
		}

		void operator *= (int valor) {
			this->valor *= valor;
		}

		void operator *= (Integer valor) {
			this->valor *= valor.getValor();
		}

		Integer operator / (int valor) {
			Integer res;
			valor /= this->valor;
			res = valor;
			return res;
		}
		
		Integer operator / (Integer valor) {
			Integer res;
			int acumulador = this->valor / valor.getValor();
			res = acumulador;
			return res;
		}

		void operator /= (int valor) {
			this->valor /= valor;
		}

		void operator /= (Integer valor) {
			this->valor /= valor.getValor();
		}

		Integer operator % (int valor) {
			Integer res;
			valor %= this->valor;
			res = valor;
			return res;
		}

		Integer operator % (Integer valor) {
			Integer res;
			int acumulador = this->valor % valor.getValor();
			res = acumulador;
			return res;
		}

		void operator %= (int valor) {
			this->valor %= valor;
		}

		void operator %= (Integer valor) {
			this->valor %= valor.getValor();
		}

		bool operator == (int valor) {
			return (this->valor == valor);
		}

		bool operator == (Integer valor) {
			return (this->valor == valor.getValor());
		}

		void operator ++ () {
			this->valor++;
		}

		void operator -- () {
			this->valor--;
		}

		bool operator < (int valor) {
			return (this->valor < valor);
		}

		bool operator < (Integer valor) {
			return (this->valor < valor.getValor ());
		}

		bool operator > (int valor) {
			return (this->valor > valor);
		}

		bool operator > (Integer valor) {
			return (this->valor > valor.getValor());
		}

		bool operator <= (int valor) {
			return (this->valor <= valor);
		}

		bool operator <= (Integer valor) {
			return (this->valor <= valor.getValor ());
		}

		bool operator >= (int valor) {
			return (this->valor >= valor);
		}

		bool operator >= (Integer valor) {
			return (this->valor >= valor.getValor());
		}

		Integer pow (int exp) {
			Integer res;
			res = 1;
			if (exp == 0 && this->valor == 0) {
				printf("\033[31;40mTanto la base como el exponente deben ser distintos de 0.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
			if (!exp)
				return res;

			for (int i = 0; i < exp; i++)
				res = res * this->valor;

			return res;
		}

		Integer pow (Integer exp) {
			return this->pow(exp.getValor());
		}

		Integer sqrtd () {
			Integer res;
			res = sqrt (this->valor);
			return res;
		}

		static long long int max () {
			return LLONG_MAX;
		}

		static long long int min () {
			return LLONG_MIN;
		}

		long long int getValor () {
			return this->valor;
		}

		static Integer parseInt (const char *frase) {
			bool esEntero = true;
			for (int i = 0; i < strlen (frase); i++)
				if (!(*(frase + i) < ':' && *(frase + i) > '/'))
					esEntero = false;

			if (esEntero) {
				Integer res;
				res = atoi (frase);
				return res;
			} else {
				printf ("\033[31;40mEl parámetro pasado a Integer::parseInt() contiene caracteres no numericos.\n\033[97;40m]]");
				exit (EXIT_FAILURE);
			}
		}

		static Integer parseInt (String frase) {
			return Integer::parseInt(frase.toString ());
		}
	private:
		char *res;
		long long int valor;
};

#endif