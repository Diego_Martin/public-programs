#ifndef __STRING_H__
#define __STRING_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Object.h"

class String : public Object
{
	public:
		String () {}
		String (const char *texto) {
			this->setValor (texto);
		}
		String (char *texto) {
			this->setValor ((const char *) texto);
		}
		String (char texto) {
			char *res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->setValor ((const char *) res);
		}

		const char *toString () {
			return (const char *) this->valor;
		}

		const char *type () {
			return "String";
		}

		// char operator [] (int index) {
		// 	if (index > -1) {
		// 		if (index < this->length()) {
		// 			char res = *(this->valor + index);
		// 			return res;
		// 		} else {
		// 			printf("\033[31;40mEl indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
		// 			exit (EXIT_FAILURE);
		// 		}
		// 	} else {
		// 		if (index > (-1 * this->length())) {
		// 			char res = *(this->valor + this->length() + index);
		// 			return res;
		// 		} else {
		// 			printf("\033[31;40mEl indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
		// 			exit (EXIT_FAILURE);
		// 		}
		// 	}
		// }

		String operator [] (int index) {
			if (index > -1) {
				if (index < this->length()) {
					char res = *(this->valor + index);
					String resu (res);
					return resu;
				} else {
					printf ("\033[31;40mEl indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->length())) {
					char res = *(this->valor + this->length() + index);
					String resu (res);
					return resu;
				} else {
					printf ("\033[31;40mEl indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

		void operator = (const char *frase) {
			this->setValor (frase);
		}

		void operator = (String frase) {
			this->setValor (frase.toString());
		}

		int length () {
			return strlen (this->valor);
		}

		void setValor (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
			// free (this->valor);
		}

		String operator + (const char *frase) {
			String res;
			char *resultado = (char *) malloc (sizeof (char) * (strlen (this->valor) + strlen (frase)));
			strcpy (resultado, this->valor);
			strcat (resultado, frase);
			res = (const char *) resultado;
			return res;
		}

		String operator + (String frase) {
			String res;
			char *resultado = (char *) malloc (sizeof (char) * (strlen (this->valor) + strlen (frase.toString ())));
			strcpy (resultado, this->valor);
			strcat (resultado, frase.toString ());
			res = (const char *) resultado;
			return res;
		}

		bool operator == (const char *frase) {
			return !((bool) strcmp ((const char *) this->valor, frase));
		}

		bool operator == (String frase) {
			return !((bool) strcmp ((const char *) this->valor, frase.toString ()));
		}

		~String () {
			delete this->valor;
		}
	private:
		char *valor;
};

#endif