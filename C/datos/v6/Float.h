#ifndef __FLOAT_H__
#define __FLOAT_H__

#include <stdio.h>
#include <stdlib.h>
#include "Object.h"

class Float : public Object
{
	public:
		Float () {}
		Float (int num) {
			this->valor = (long double) num;
		}
		Float (float num) {
			this->valor = (long double) num;
		}
		Float (double num) {
			this->valor = (long double) num;
		}
		Float (long double num) {
			this->valor = num;
		}

		const char *toString () {
			char *res = (char *) malloc (sizeof (char) * 35);
			sprintf (res, "%Lf", this->valor);
			return (const char *) res;
		}

		const char *type () {
			return "Float";
		}
		void setValor (const char *frase) {return;}

		double getValor () {
			return this->valor;
		}
	private:
		long double valor;
};

#endif