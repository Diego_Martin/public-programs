#ifndef __OBJECT_H__
#define __OBJECT_H__

/*
=========================================
==            Clase Object             ==
=========================================
== Es la clase padre de todos los ti-  ==
== pos de datos que voy a crear.       ==
==                                     ==
== Realmente se podría decir que no es ==
== una clase, sino una interfaz (clase ==
== 100% abstracta)                     ==
==                                     ==
== Todos los metodos de esta clase     ==
== deben ser sobreescritos pero es ne- ==
== cesaria.                            ==
=========================================
*/
class Object
{
	public:
		// Object();
		// ~Object();
		virtual const char *toString () = 0;
		virtual const char *type () = 0;
		virtual void setValor (const char *frase) = 0;
};

#endif