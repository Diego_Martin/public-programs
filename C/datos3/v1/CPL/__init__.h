#ifndef __CPL_H__
#define __CPL_H__

#define __eq__   operator==
#define __le__   operator<=
#define __ge__   operator>=
#define __lt__   operator<
#define __gt__   operator>
#define __not__  operator!
#define __hash__ operator[]
#define __call__ operator()
#define __add__  operator+
#define __subs__ operator-
#define __mul__  operator*
#define __div__  operator/
#define __and__  operator&&
#define __or__   operator||
#define __as__   operator=
// #define . ::
#define NotImplemented(x) _NotImplemented(__PRETTY_FUNCTION__, x)

#include <stdio.h>
#include <stdlib.h>

void _NotImplemented (const char *fun = __PRETTY_FUNCTION__, const char *msg = "No está implementado aún.") {
	printf ("%s - %s\n", fun, msg);
	exit (EXIT_FAILURE);
}

#include "core/interfaces/__init__.h"
#include "core/clases/__init__.h"
#include "core/excepciones/__init__.h"
#include "basics/__init__.h"

#endif