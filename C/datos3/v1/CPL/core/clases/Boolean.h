#ifndef __BOOLEAN_H__
#define __BOOLEAN_H__

#include "../interfaces/Object.h"
#include <string.h>

class Boolean : public Object
{
	public:
		Boolean () {
			this->valor = false;
		}

		Boolean (bool valor) {
			this->valor = valor;
		}

		Boolean (int valor) {
			this->valor = (bool) valor;
		}

		~Boolean();

		const char *__repr__ () {
			return (this->valor) ? "True" : "False";
		}

		const char *__name__ () {
			return "Boolean";
		}

		unsigned long long int __sizeof__ () {
			return sizeof (*this);
		}

		bool __getcvalue__ () {
			return this->valor;
		}

		Boolean *__cvalue__ () {
			return this;
		}

		const char *__doc__ = "Clase que representa al tipo de dato 'Booleano' (bool).\n\n";

		void __as__ (bool valor) {
			this->valor = valor;
		}

		void __as__ (Boolean valor) {
			this->valor = valor.__getcvalue__ ();
		}

		void __as__ (Boolean *valor) {
			this->valor = valor->__getcvalue__ ();
		}

		bool __eq__ (bool valor) {
			return (bool)(this->valor == valor);
		}

		bool __eq__ (Boolean valor) {
			return (bool)(this->valor == valor.__getcvalue__ ());
		}

		bool __eq__ (Boolean *valor) {
			return (bool)(this->valor == valor->__getcvalue__ ());
		}

		bool __and__ (bool valor) {
			return (bool)(this->valor && valor);
		}

		bool __and__ (Boolean valor) {
			return (bool)(this->valor && valor.__getcvalue__ ());
		}

		bool __and__ (Boolean *valor) {
			return (bool)(this->valor && valor->__getcvalue__ ());
		}

		bool __or__ (bool valor) {
			return (bool)(this->valor || valor);
		}

		bool __or__ (Boolean valor) {
			return (bool)(this->valor || valor.__getcvalue__ ());
		}

		bool __or__ (Boolean *valor) {
			return (bool)(this->valor || valor->__getcvalue__ ());
		}

		bool __not__ () {
			return (bool)(!this->valor);
		}

	private:
		bool valor;
};

#endif