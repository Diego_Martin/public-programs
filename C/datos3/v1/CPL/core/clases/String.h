#ifndef __STRING_H__
#define __STRING_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../interfaces/Object.h"
#include "../interfaces/Iterable.h"
#include "Boolean.h"
#include "Integer.h"
#include "None.h"

class String : public Object, public Iterable
{
	public:
		String () {
			this->__is_iterable__ = true;
			this->__setstr__ ("");
		}

		String (const char *texto) {
			this->__is_iterable__ = true;
			this->__setstr__ (texto);
		}

		String (char *texto) {
			this->__is_iterable__ = true;
			this->__setstr__ ((const char *) texto);
		}

		String (char texto) {
			this->__is_iterable__ = true;
			this->res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->__setstr__ ((const char *) this->res);
		}

		~String () {
			free (this->valor);
			free (this->res);
		}

		const char *__name__ () {
			return "String";
		}

		const char *__repr__ () {
			return (const char *) this->valor;
		}

		const char *__str__ () {
			this->res = (char *) malloc (sizeof (char) * (strlen (this->valor) + 2));
			sprintf (res, "'%s'", this->valor);
			return (const char *) this->res;
		}

		void __setstr__ (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
		}

		unsigned long long int __len__ () {
			return (unsigned long long int) strlen (this->valor);
		}

		void each () {
			this->__index__ = 0;
		}

		bool hasNext () {
			return (this->__index__ < (this->length () - 1));
		}

		Object *next () {
			this->__index__ ++;
			return this->__hash__ (new Integer ((this->__index__ - 1)));
		}

		unsigned long long int __sizeof__ () {
			return sizeof (*this);
		}

		Object *get () {
			if (this->__index__ > -1) {
				if (this->__index__ < this->length ()) {
					char res = *(this->valor + this->__index__);
					return new String (res);
				} else {
					printf ("\033[31;40mERROR: El indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (this->__index__ > (-1 * this->length ())) {
					char res = *(this->valor + this->length () + this->__index__);
					return new String (res);
				} else {
					printf ("\033[31;40mERROR: El indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

		String *__hash__ (int index) {
			return new String (this->get (index));
		}

		String *__hash__ (Integer index) {
			return new String (this->get (index.__getcvalue__ ()));
		}

		Object *__hash__ (Object *index) {
			if (strcmp (index->__name__ (), "Integer") == 0)
				return new String (this->get (((Integer *)index)->__getcvalue__ ()));
			else {
				printf ("\033[31;40mERROR: El indice debe ser un número entero (Integer) o un puntero a un número entero (Integer *).\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
		}

		Object *__slice__ (Integer *start, Integer *end = new Integer (-1), Integer *step = new Integer (1)) {
			return NULL;
		}

		int length () {
			return strlen (this->valor);
		}

		String *__add__ (String *valor) {
			stringstream canal;
			canal << this->__repr__ () << valor->__repr__ ();
			const string& res = canal.str ();
			return new String (res.c_str ());
		}

		String *__add__ (String valor) {
			return this->operator+ (&valor);
		}

		String *__add__ (const char *valor) {
			return this->operator+ (new String (valor));
		}

		String *__add__ (const String& valor) {
			return this->operator+ ((String *) &valor);
		}

		None *__as__ (String *valor) {
			this->__setstr__ (valor->__repr__ ());
			return new None ();
		}

		None *__as__ (String valor) {
			this->operator= (&valor);
			return new None ();
		}

		None *__as__ (const char *valor) {
			this->operator= (new String (valor));
			return new None ();
		}

		None *__as__ (const String& valor) {
			this->operator= ((String *) &valor);
			return new None ();
		}

		String *__mul__ (int multiplicador) {
			if (multiplicador == 0)
				return new String ();

			stringstream canal;
			canal << "";

			for (int i = 0; i < multiplicador; i++)
				canal << this->__repr__ ();
			
			const string& res = canal.str ();
			return new String (res.c_str ());
		}

		String *__mul__ (Integer multiplicador) {
			return this->__mul__ (multiplicador.__getcvalue__ ());
		}

		String *__mul__ (Integer *multiplicador) {
			return this->__mul__ (multiplicador->__getcvalue__ ());
		}
	private:
		char *valor;
		char *res;
		int __index__;
		bool __is_iterable__;

		char get (int index) {
			if (index > -1) {
				if (index < this->length()) {
					char res = *(this->valor + index);
					return res;
				} else {
					printf ("\033[31;40mERROR: El indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->length())) {
					char res = *(this->valor + this->length() + index);
					return res;
				} else {
					printf ("\033[31;40mERROR: El indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

};


#endif