#ifndef __ITERABLE_H__
#define __ITERABLE_H__

#include "Object.h"
#include "../clases/Integer.h"

class Iterable
{
	public:
		bool __is_iterable__ = true;
		virtual unsigned long long int __len__ () = 0;
		virtual void each () = 0;
		virtual bool hasNext () = 0;
		virtual Object *next () = 0;
		virtual Object *get () = 0;
		virtual Object *__hash__ (Object *index) = 0;
		virtual Object *__slice__ (Integer *start, Integer *end = new Integer (-1), Integer *step = new Integer (1)) = 0;

	protected:
		int __index__;
};

#endif