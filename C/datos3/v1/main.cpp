#include "CPL/__init__.h"
#include <stdio.h>

class Prueba : public Object
{
public:
	void si () {
		printf ("Si\n");
	}

	const char *__name__ () {
		return "Prueba";
	}
	// Prueba();
	// ~Prueba();
	
};


int main (int argc, char const *argv[])
{
	Prueba *prueba = new Prueba ();
	prueba->si ();
	List *array = new List ();
	array->append (new String ("Hola Jose Luis"));
	array->append (new Integer (73));
	array->append (new Boolean (true));
	array->append (new Float (3.14));
	print (array);
	printf ("%llu\n", array->__len__ ());
	for (int i = 0; i < array->__len__ (); i++)
		print ((*array) [i]);

	print (new String ("Hola"), new Integer (3), new Boolean (true));
	print (prueba);
	File *archivo = new File ("./CPL/archivo.txt");
	// print (len ((Iterable *) (*array) [2])); // No funciona len ()
	// printf ("%llu\n", ((String *) ((*array) [0]))->__len__ ());
	print (new String ("El fichero 'archivo' ocupa:"), Sys::Sizeof (archivo));
	return 0;
}
