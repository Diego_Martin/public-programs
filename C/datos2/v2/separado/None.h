#ifndef __NONE_H__
#define __NONE_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "Object.h"
#include <stdlib.h>

class None : public Object
{
	public:
		None () {
			this->valor = NULL;
		}

		const char *__repr__ () {
			return "None";
		}

		const char *__name__ () {
			return "None";
		}

		int __len__ () {
			return 0;
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __size__ () {
			return sizeof (*this);
		}

		static Object *nada () {
			Object *res (NULL);
			return res;
		}
	private:
		void *valor;
};

#ifdef __cplusplus
}
#endif

#endif