#ifndef __DICT_H__
#define __DICT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "Object.h"

class Dict : public Object
{
	public:
		Dict();
		~Dict();

		const char *__repr__ () {
			// TODO
			return "";
		}

		const char *__name__ () {
			return "Dict";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->len;
		}

		int __size__ () {
			return sizeof (*this);
		}

	private:
		Array valor;
		Array clave;
		int len;
};

#ifdef __cplusplus
}
#endif

#endif