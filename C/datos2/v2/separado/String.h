#ifndef __STRING_H__
#define __STRING_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Object.h"
#include "Boolean.h"
#include "None.h"

class String : public Object
{
	public:
		String () {
			this->__setstr__ ("");
		}
		String (const char *texto) {
			this->__setstr__ (texto);
		}
		String (char *texto) {
			this->__setstr__ ((const char *) texto);
		}
		String (char texto) {
			char *res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->__setstr__ ((const char *) res);
			free (res);
		}
		~String () {
			free (this->valor);
		}

		const char *__repr__ () {
			return (const char *) this->valor;
		}

		const char *__name__ () {
			return "String";
		}

		void __setstr__ (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
		}

		int __len__ () {
			return strlen (this->valor);
		}

		int __size__ () {
			return sizeof (*this);
		}
/*
		char operator [] (int index) {
			return (*this).get (index);
		}*/

		String *operator [] (int index) {
			return new String ((*this).get (index));
		}

		char get (int index) {
			if (index > -1) {
				if (index < this->length()) {
					char res = *(this->valor + index);
					return res;
				} else {
					printf ("\033[31;40mERROR: El indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->length())) {
					char res = *(this->valor + this->length() + index);
					return res;
				} else {
					printf ("\033[31;40mERROR: El indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

		int length () {
			return strlen (this->valor);
		}

	private:
		char *valor;
};

#ifdef __cplusplus
}
#endif

#endif