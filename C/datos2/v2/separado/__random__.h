#ifndef __RANDOM_H__
#define __RANDOM_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "__datos__.h"
#include <time.h>
#include <stdlib.h>

namespace Random
{
	Integer *RandInt (Integer *max) {
		if (max->__getcvalue__ () == 0) {
			fprintf (stderr, "\033[31;40mERROR: Los números aleatorios entre: 0, y: 0, sólo devuelven un resultado: 0. Eres un@ cazurr@.\033[97;40m");
			exit (1);
		}

		srand (time (NULL));
		return new Integer (rand () % max->__getcvalue__ () + 1);
	}

	Integer *RandInt (Integer *min, Integer *max) {
		if (max->__getcvalue__ () == min->__getcvalue__ ()) {
			fprintf (stderr, "\033[31;40mERROR: Los números aleatorios entre: %lli, y: %lli, sólo devuelven un resultado: %lli. Eres un@ cazurr@.\033[97;40m", min->__getcvalue__ (), min->__getcvalue__ (), min->__getcvalue__ ());
			exit (1);
		}

		srand (time (NULL));
		return new Integer ((rand () % (max->__getcvalue__ () - min->__getcvalue__ () + 1)) + min->__getcvalue__ ());
	}

	Object *Choice (Array *source) {
		if (source->__len__ () == 0) {
			fprintf (stderr, "\033[31;40mERROR: La longitud del Array es 0.\033[97;40m");
			exit (1);
		}

		srand (time (NULL));
		return source->operator[] (rand () % source->__len__ ());
	}

}

#ifdef __cplusplus
}
#endif

#endif