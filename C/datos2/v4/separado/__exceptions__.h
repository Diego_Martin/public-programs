#ifndef __EXCEPTIONS_H__
#define __EXCEPTIONS_H__

#include "IndexAlreadyExistsException.h"
#include "IndexNotInDictException.h"
#include "IndexOutOfBoundsException.h"

#endif