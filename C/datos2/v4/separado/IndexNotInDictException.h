#ifndef __INDEXNOTINDICTEXCEPTION_H__
#define __INDEXNOTINDICTEXCEPTION_H__

#include <exception>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include "Object.h"
#include "Exception.h"

class IndexNotInDictException : public Exception
{
	public:
		IndexNotInDictException (Object *index, Object *notin, Object *emisor, const char *mensaje = NULL, const char *archivo = __FILE__, int linea = __LINE__) : Exception (mensaje, archivo, linea) {
			char *strlinea = NULL;
			if (mensaje == NULL) {
				std::stringstream canal;
				canal << "\033[91;40mException (" << this->__name__ () << ") released at:\n\033[97;40mLine: \033[92;40m" << linea << "\n\033[97;40mFile: \033[92;40m" << archivo << "\n\033[93;40mMessage: " << mensaje << ". Key: " << notin->__repr__ () << " not exists in " << emisor->__name__ () << " of length: " << index->__repr__ () << ".\n\033[97;40m";
				const std::string& tmp = canal.str();
				strlinea = (char *) tmp.c_str ();
			} else {
				strlinea = (char *) malloc (sizeof(char) * strlen (mensaje));
				strcpy (strlinea, mensaje);
			}
			this->mensaje = (char *) malloc (sizeof (char) * strlen (strlinea));
			strcpy (this->mensaje, strlinea);
			free ((void *) strlinea);
		}

		~IndexNotInDictException () {
			free (this->mensaje);
		}

		const char *what () const throw () {
			return (const char *) mensaje;
		}

		const char *__repr__ () {
			return this->what ();
		}

		const char *__name__ () {
			return "IndexNotInDictException";
		}

		int __size__ () {
			return sizeof (*this);
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

		virtual const char *get_message () {
			return this->what ();
		}

		virtual void print_stack_trace () {
			fprintf (stderr, "%s", this->what ());
		}

	private:
		char *mensaje = NULL;
};

#endif