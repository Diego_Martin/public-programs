#ifndef __SYS_H__
#define __SYS_H__

#include "__datos__.h"
#include "__misc__.h"
#include <stdio.h>
#include <stdlib.h>

namespace Sys
{		
	File *Stdin  = new File (stdin);
	File *Stdout = new File (stdout);
	File *Stderr = new File (stderr);
	File *Stdvbs = new File (stdout);

	None *Exit (Integer *estado = new Integer (0)) {
		exit (estado->__getcvalue__ ());
	}

	None *Exit (String *mensaje, File *canal = Sys::Stdout) {
		// printf ("%s\n", mensaje->__repr__ ());
		fprintf (canal->__getcfile__ (), "%s\n", mensaje->__repr__ ());
		exit (0);
	}

	Integer *Sizeof (String *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (Integer *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (Boolean *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (Float *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (None *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (File *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (Array *variable) {
		return new Integer (variable->__size__ ());
	}

	Integer *Sizeof (Dict *variable) {
		return new Integer (variable->__size__ ());
	}
};

#endif