#ifndef __TUPLE_H__
#define __TUPLE_H__

#include "Object.h"
#include "Integer.h"
#include "Array.h"
#include "IndexOutOfBoundsException.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
using namespace std;

class Tuple
{
	public:
		Tuple () {
			this->len = 0;
		}
		~Tuple () {
			delete &(this->valor);
		}

		const char *__name__ () {
			return "Tuple";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->valor.__len__ ();
		}

		int __size__ () {
			return sizeof (*this);
		}

		Object * operator [] (int index) {
			return this->valor [index];
		}

		Object * operator [] (Integer *index) {
			return (*this) [(int) index->__getcvalue__ ()];
		}

	private:
		Array valor;
		int len;
};

#endif