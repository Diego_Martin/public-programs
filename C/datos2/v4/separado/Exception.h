#ifndef __EXCEPTION_H__
#define __EXCEPTION_H__

#include <exception>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include "Throwable.h"

class Exception : public std::runtime_error, public Throwable
{
	public:
		Exception (const char *mensaje = NULL, const char *archivo = __FILE__, int linea = __LINE__) : std::runtime_error ("") {
			char *strlinea = NULL;
			if (mensaje == NULL) {
				std::stringstream canal;
				canal << "\033[91;40mException (" << this->__name__ () << ") released at:\n\033[97;40mLine: \033[92;40m" << linea << "\n\033[97;40mFile: \033[92;40m" << archivo << "\n\033[93;40mMessage: " << mensaje << ".\n\033[97;40m";
				const std::string& tmp = canal.str();
				strlinea = (char *) tmp.c_str ();
			} else {
				strlinea = (char *) malloc (sizeof(char) * strlen (mensaje));
				strcpy (strlinea, mensaje);
			}

			this->mensaje = (char *) malloc (sizeof (char) * strlen (strlinea));
			strcpy (this->mensaje, strlinea);
			free ((void *) strlinea);
		}

		~Exception () {
			free (this->mensaje);
		}

		const char *what () const throw () {
			return (const char *) mensaje;
		}

		const char *__repr__ () {
			return this->what ();
		}

		const char *__name__ () {
			return "Exception";
		}

		int __size__ () {
			return sizeof (*this);
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

		virtual const char *get_message () {
			return this->what ();
		}

		virtual void print_stack_trace () {
			fprintf (stderr, "%s", this->what ());
		}

	private:
		char *mensaje = NULL;
};

#endif