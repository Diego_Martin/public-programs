#ifndef __DATOS_H__
#define __DATOS_H__

#include "Object.h"
#include "Throwable.h"
#include "Error.h"
#include "Exception.h"
#include "None.h"
#include "Boolean.h"
#include "String.h"
#include "Integer.h"
#include "Float.h"
#include "Array.h"
#include "Dict.h"
#include "File.h"
#include "Tuple.h"

#endif