#ifndef __FLOAT_H__
#define __FLOAT_H__

#include "Object.h"
#include <string.h>
#include <sstream>
using namespace std;

class Float : public Object
{
	public:
		Float () {
			this->valor = (long double) 0;
		}

		Float (int num) {
			this->valor = (long double) num;
		}

		Float (float num) {
			this->valor = (long double) num;
		}

		Float (double num) {
			this->valor = (long double) num;
		}

		Float (long double num) {
			this->valor = num;
		}

		~Float();

		const char *__repr__ () {
			stringstream ss;
			ss << this->valor;
			const char *str = ss.str ().c_str ();
			return str;
		}

		const char *__name__ () {
			return "Float";
		}

		long double __getcvalue__ () {
			return this->valor;
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

		int __size__ () {
			return sizeof (*this);
		}

	private:
		long double valor;
};

#endif