#ifndef __ARRAY_H__
#define __ARRAY_H__

#include "Object.h"
#include "Integer.h"
#include "IndexOutOfBoundsException.h"
#include "None.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
using namespace std;

class Array : public Object
{
	public:
		Array () {
			this->len = 0;
			this->valor = (Object **) malloc (sizeof (Object *) * this->len);
		}
		~Array () {
			free (this->valor);
		}

		const char *__repr__ () { // DONEEEE
			stringstream canal;
			canal << "[";
			for (int i = 0; i < this->len; i++) {
				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";

				canal << this->operator[] (i)->__repr__ ();

				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";
				
				if (i < this->len - 1)
					canal << ", ";
			}
			canal << "]";
			const string& res = canal.str ();
			char *ret = (char *) malloc (sizeof (char) * strlen (res.c_str ()));
			strcpy(ret, res.c_str ());
			return (const char *) ret;
		}

		const char *__name__ () {
			return "Array";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->len;
		}

		int __size__ () {
			return sizeof (*this);
		}

		Object *operator [] (int index) {
			if (index < this->len && index > -1)
				return *(valor + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(valor + (-1 * index));
			else
				throw new IndexOutOfBoundsException (index, this->__len__ ());
		}

		Object *operator [] (Integer *index) {
			return &this [(int) index->__getcvalue__ ()];
		}

		None *append (Object *elemento) {
			this->__grow__ ();
			*(this->valor + this->len - 1) = elemento;
			return new None ();
		}

		None *append (Object *elemento, Object *elemento2) {
			this->__grow__ (2);
			*(this->valor + this->len - 2) = elemento;
			*(this->valor + this->len - 1) = elemento2;
			return new None ();
		}

	private:
		Object **valor;
		int len;

		void __grow__ (int size = 1) {
			this->len += size;
			this->valor = (Object **) realloc (this->valor, sizeof (Object *) * this->len);
		}
};

#endif