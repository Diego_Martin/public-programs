#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>

class Object
{
	public:
		virtual const char *__repr__ () = 0;
		virtual const char *__type__ () = 0;
		virtual int __len__ () = 0;
		virtual void __setstr__ (const char *frase) = 0;
};

class None : public Object
{
	public:
		None () {
			this->valor = NULL;
		}

		const char *__repr__ () {
			return "None";
		}

		const char *__type__ () {
			return "None";
		}

		int __len__ () {
			return 0;
		}

		void __setstr__ (const char *frase) {
			return;
		}

		static Object *nada () {
			Object *res (NULL);
			return res;
		}
	private:
		void *valor;
};

class String : public Object
{
	public:
		String () {
			this->__setstr__ ("");
		}
		String (const char *texto) {
			this->__setstr__ (texto);
		}
		String (char *texto) {
			this->__setstr__ ((const char *) texto);
		}
		String (char texto) {
			char *res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->__setstr__ ((const char *) res);
		}
		~String();

		const char *__repr__ () {
			return (const char *) this->valor;
		}

		const char *__type__ () {
			return "String";
		}

		void __setstr__ (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
		}

		int __len__ () {
			return strlen (this->valor);
		}

		/*String *operator [] (int index) {
			if (index > -1 && index < this->__len__ ())
			else if (index < 0 && index >= (this->__len__ () * -1))
			else {

				exit (EXIT_FAILURE);
			}
		}*/
		String *operator [] (int index) {
			if (index > -1) {
				if (index < this->__len__ ()) {
					char res = *(this->valor + index);
					String *resu = new String (res);
					return resu;
				} else {
					fprintf (stderr, "\033[31;40mEl indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->__len__ ())) {
					char res = *(this->valor + this->__len__ () + index);
					String *resu = new String (res);
					return resu;
				} else {
					fprintf (stderr, "\033[31;40mEl indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

	private:
		char *valor;
};

class Boolean : public Object
{
	public:
		Boolean () {
			this->valor = false;
		}

		Boolean (bool valor) {
			this->valor = valor;
		}

		Boolean (int valor) {
			this->valor = (bool) valor;
		}

		~Boolean();

		const char *__repr__ () {
			return (this->valor) ? "True" : "False";
		}

		const char *__type__ () {
			return "Boolean";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

	private:
		bool valor;
};

class Integer : public Object
{
	public:
		Integer () {
			this->valor = 0;
		}

		Integer (int num) {
			this->valor = (long long int) num;
		}

		Integer (float num) {
			this->valor = (long long int) num;
		}

		Integer (double num) {
			this->valor = (long long int) num;
		}

		Integer (long int num) {
			this->valor = (long long int) num;
		}

		Integer (long long int num) {
			this->valor = num;
		}

		Integer (Integer *obj)  {
			this->valor = obj->__get_valor__ ();
		}

		~Integer();

		const char *__repr__ () {
			int contador = 1;
			long long int aux = this->valor;
			char *res = NULL;
			
			if (aux >= 0) {
				while(aux / 10 > 0) {
					aux /= 10;
					contador++;
				}
				res = (char *) malloc (sizeof (char) * contador);
			} else {
				while(aux * 10 > 0) {
					aux *= 10;
					contador++;
				}
				res = (char *) malloc (sizeof (char) * (contador + 1));
			}
			sprintf (res, "%lli", this->valor);
			return (const char *) res;
		}

		const char *__type__ () {
			return "Integer";
		}

		void operator = (int valor) {
			this->valor = valor;
		}

		void operator = (Integer valor) {
			this->valor = valor.__get_valor__ ();
		}

		Integer operator + (int valor) {
			Integer res;
			valor += this->valor;
			res = valor;
			return res;
		}

		Integer operator + (Integer valor) {
			return this->operator+(valor.__get_valor__ ());
		}

		long long int __get_valor__ () {
			return this->valor;
		}

		static Integer parseInt (const char *frase) {
			bool esEntero = true;
			for (int i = 0; i < strlen (frase); i++)
				if (!(*(frase + i) < ':' && *(frase + i) > '/'))
					esEntero = false;

			if (esEntero) {
				Integer res;
				res = atoi (frase);
				return res;
			} else {
				printf ("\033[31;40mEl parámetro pasado a Integer::parseInt() contiene caracteres no numericos.\n\033[97;40m]]");
				exit (EXIT_FAILURE);
			}
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

	private:
		long long int valor;
};

class Float : public Object
{
	public:
		Float () {
			this->valor = (long double) 0;
		}

		Float (int num) {
			this->valor = (long double) num;
		}

		Float (float num) {
			this->valor = (long double) num;
		}

		Float (double num) {
			this->valor = (long double) num;
		}

		Float (long double num) {
			this->valor = num;
		}

		~Float();

		const char *__repr__ () {
			std::stringstream ss;
			ss << this->valor;
			const char *str = ss.str().c_str();
			return str;
		}

		const char *__type__ () {
			return "Float";
		}

		long double __get_valor__ () {
			return this->valor;
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return strlen (this->__repr__ ());
		}

	private:
		long double valor;
};

class Array : public Object
{
	public:
		Array () {
			this->len = 0;
			this->valor = (Object **) malloc (sizeof (Object *) * this->len);
		}
		~Array () {
			free (this->valor);
		}

		const char *__repr__ () {
			// TODO
			return "";
		}

		const char *__type__ () {
			return "Array";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->len;
		}

		Object *operator [] (int index) {
			if (index < this->len && index > -1)
				return *(valor + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(valor + (-1 * index));
			else {
				printf ("\033[31;40mEl indice debe ser menor que la longitud de la lista y mayor que la longitud de la lista en negativo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
		}

		Object *operator [] (Integer *index) {
			return &this [(int) index->__get_valor__ ()];
		}

	private:
		Object **valor;
		int len;

		void __grow__ (int size = 1) {
			this->len += size;
			this->valor = (Object **) realloc (this->valor, sizeof (Object *) * this->len);
		}
};

class Dict : public Object
{
	public:
		Dict();
		~Dict();

		const char *__repr__ () {
			// TODO
			return "";
		}

		const char *__type__ () {
			return "Dict";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->len;
		}

	private:
		Array valor;
		Array clave;
		int len;
};

None *print (Object *variable, String *end = new String ("\n")) {
	printf ("%s%s", variable->__repr__ (), end->__repr__ ());
}

Integer *len (Object *variable) {
	return new Integer (variable->__len__ ());
}

String *type (Object *variable) {
	return new String (variable->__type__ ());
}

int main (int argc, char *argv []) {
	Object *var (NULL); // Ignora esto

	var = new Integer (-3);
	print (var);

	var = new Boolean (true);
	print (var);

	var = new String ("Hola mundo.");
	print (var);
	print (new String ("Longitud: "), new String (""));
	print (len (var));

	var = new Float (3.1415);
	print (var);

	var = new None ();
	print (var);

	return EXIT_SUCCESS;
}