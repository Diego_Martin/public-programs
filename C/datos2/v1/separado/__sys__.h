#ifndef __SYS_H__
#define __SYS_H__

#include "__datos__.h"
#include <stdio.h>
#include <stdlib.h>

namespace Sys
{		
	File *Stdin  = new File (stdin);
	File *Stdout = new File (stdout);
	File *Stderr = new File (stderr);
	File *Stdvbs = new File (stdout);

	None *_exit (Integer *estado = new Integer (0)) {
		exit (estado->__getcvalue__ ());
	}

	None *_exit (String *mensaje) {
		printf ("%s\n", mensaje->__repr__ ());
		exit (0);
	}
};

#endif