#ifndef __ARRAY_H__
#define __ARRAY_H__

#include "Object.h"
#include "Integer.h"
#include "None.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Array : public Object
{
	public:
		Array () {
			this->len = 0;
			this->valor = (Object **) malloc (sizeof (Object *) * this->len);
		}
		~Array () {
			free (this->valor);
		}

		const char *__repr__ () {
			// TODO
			int tamano = sizeof (char);
			char *res = (char *) malloc (tamano);
			strcpy (res, "[");

			for (int i = 0; i < this->len; i++) {
				tamano += sizeof (char) * strlen ((*(this->valor) + (i * sizeof (Object *)))->__repr__ ());
				res = (char *) realloc (res, tamano);
				strcat (res, (*(this->valor) + (i * sizeof (Object *)))->__repr__ ());

				if (i < this->len - 1) {
					tamano += sizeof (char) * 2;
					res = (char *) realloc (res, tamano);
					strcat (res, ", ");
				}
			}

			tamano += sizeof (char);
			res = (char *) realloc (res, tamano);
			strcat (res, "]");
			return (const char *) res;
		}

		const char *__name__ () {
			return "Array";
		}

		void __setstr__ (const char *frase) {
			return;
		}

		int __len__ () {
			return this->len;
		}

		Object *operator [] (int index) {
			if (index < this->len && index > -1)
				return *(valor + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(valor + (-1 * index));
			else {
				printf ("\033[31;40mERROR: El indice debe ser menor que la longitud de la lista y mayor que la longitud de la lista en negativo.\n\033[97;40m");
				exit (EXIT_FAILURE);
			}
		}

		Object *operator [] (Integer *index) {
			return &this [(int) index->__getcvalue__ ()];
		}

		None *append (Object *elemento) {
			this->__grow__ ();
			*(this->valor + this->len - 1) = elemento;
			return new None ();
		}

	private:
		Object **valor;
		int len;

		void __grow__ (int size = 1) {
			this->len += size;
			this->valor = (Object **) realloc (this->valor, sizeof (Object *) * this->len);
		}
};

#endif