#ifndef __NONE_H__
#define __NONE_H__

#include "Object.h"
#include <stdlib.h>

class None : public Object
{
	public:
		None () {
			this->valor = NULL;
		}

		const char *__repr__ () {
			return "None";
		}

		const char *__name__ () {
			return "None";
		}

		int __len__ () {
			return 0;
		}

		void __setstr__ (const char *frase) {
			return;
		}

		static Object *nada () {
			Object *res (NULL);
			return res;
		}
	private:
		void *valor;
};

#endif