#ifndef __STRING_H__
#define __STRING_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Object.h"
#include "Boolean.h"
#include "None.h"

class String : public Object
{
	public:
		String () {
			this->__setstr__ ("");
		}
		String (const char *texto) {
			this->__setstr__ (texto);
		}
		String (char *texto) {
			this->__setstr__ ((const char *) texto);
		}
		String (char texto) {
			char *res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->__setstr__ ((const char *) res);
			free (res);
		}
		~String () {
			free (this->valor);
		}

		const char *__repr__ () {
			return (const char *) this->valor;
		}

		const char *__name__ () {
			return "String";
		}

		void __setstr__ (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
		}

		int __len__ () {
			return strlen (this->valor);
		}

	private:
		char *valor;
};

#endif