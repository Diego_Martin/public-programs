#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Object
{
	public:
		virtual const char *__repr__ () {
			char *res = (char *) malloc (sizeof (char) * (13 + strlen (this->__name__ ()) + 14));
			sprintf (res, "<%s class at %p>", this->__name__ (), (void *) this);
			return (const char *) res;
		}

		virtual const char *__name__ () {
			return "Object";
		}

		virtual int __len__ () {
			return 0;
		}

		virtual void __setstr__ (const char *frase) {
			return;
		}
};

#endif