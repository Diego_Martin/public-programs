#ifndef __TYPE_H__
#define __TYPE_H__

#include "Object.h"
#include "List.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class _Type : public Object {
	public:
		_Type () {}
		~_Type () {
			free (this->res);
		}

		const char *__name__ () {
			return "Type";
		}

		// const char *__repr__ () {
		// 	this->res = (char *) malloc (sizeof (char) * (12 + strlen (this->__name__ ()) + 14));
		// 	sprintf (this->res, "<clase %s en %p>", this->__name__ (), (void *) this);
		// 	return (const char *) this->res;
		// }

		unsigned long long int __sizeof__ () {
			return (unsigned long long int)(sizeof (char) * strlen (this->__repr__ ()));
		}

		_Type *__cvalue__ () {
			return this;
		}
		
		const char * operator () (Object *dato) {
			this->res = (char *) malloc (sizeof (char) * (100));
			sprintf (this->res, "<clase '%s'>", dato->__name__ ());
			return (const char *) this->res;
		}

		const char *__doc__ = "Objeto tipo de dato (type):\n\nProvee de información acerca del objeto.";

	private:
		char *res = NULL;
};

_Type *Type = new _Type ();

#endif