#ifndef __INTEGER_H__
#define __INTEGER_H__

#include "Object.h"
#include "List.h"
#include "Boolean.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Integer : public Object
{
	public:
		Integer () {
			this->valor = 0;
		}

		Integer (int num) {
			this->valor = (long long int) num;
		}

		Integer (float num) {
			this->valor = (long long int) num;
		}

		Integer (double num) {
			this->valor = (long long int) num;
		}

		Integer (long int num) {
			this->valor = (long long int) num;
		}

		Integer (long long int num) {
			this->valor = num;
		}

		Integer (Integer *obj)  {
			this->valor = obj->__getcvalue__ ();
		}

		~Integer () {
			// delete &(this->valor);
		}

		const char *__name__ () {
			return "Integer";
		}

		const char *__repr__ () {
			int contador = 1;
			long long int aux = this->valor;
			char *res = NULL;
			
			if (aux >= 0) {
				while (aux / 10 > 0) {
					aux /= 10;
					contador++;
				}
				res = (char *) malloc (sizeof (char) * contador);
			} else {
				while (aux * 10 > 0) {
					aux *= 10;
					contador++;
				}
				res = (char *) malloc (sizeof (char) * (contador + 1));
			}
			sprintf (res, "%lli", this->valor);
			return (const char *) res;
		}

		// const char *__str__ () {}

		unsigned long long int __sizeof__ () {
			return sizeof (this->valor);
		}

		Integer *__cvalue__ () {
			return this;
		}
		
		long long int __getcvalue__ () {
			return this->valor;
		}

		const char *__doc__ = "Clase que representa el tipo de dato 'Numero entero' (int).\n\n";

		// Sobrecarga de operadores
		void __as__ (long long int valor) {
			this->valor = valor;
		}

		void __as__ (Integer valor) {
			this->valor = valor.__getcvalue__ ();
		}

		void __as__ (Integer *valor) {
			this->valor = valor->__getcvalue__ ();
		}

		Integer *__add__ (int valor) {
			Integer *res = new Integer ();
			valor += this->valor;
			*res = valor;
			return res;
		}

		Integer *__add__ (Integer valor) {
			return this->__add__(valor.__cvalue__ ());
		}

		Integer *__add__ (Integer *valor) {
			return this->__add__(valor->__cvalue__ ());
		}

		Integer *__subs__ (int valor) {
			Integer *res = new Integer ();
			valor -= this->valor;
			*res = valor;
			return res;
		}

		Integer *__subs__ (Integer valor) {
			return this->__subs__(valor.__cvalue__ ());
		}

		Integer *__subs__ (Integer *valor) {
			return this->__subs__(valor->__cvalue__ ());
		}

		Integer *__mul__ (int valor) {
			Integer *res = new Integer ();
			valor *= this->valor;
			*res = valor;
			return res;
		}

		Integer *__mul__ (Integer valor) {
			return this->__mul__(valor.__cvalue__ ());
		}

		Integer *__mul__ (Integer *valor) {
			return this->__mul__(valor->__cvalue__ ());
		}

		Integer *__div__ (int valor) {
			Integer *res = new Integer ();
			valor /= this->valor;
			*res = valor;
			return res;
		}

		Integer *__div__ (Integer valor) {
			return this->__div__(valor.__cvalue__ ());
		}

		Integer *__div__ (Integer *valor) {
			return this->__div__(valor->__cvalue__ ());
		}

		Boolean *__eq__ (int valor) {
			return new Boolean ((bool) (this->valor == valor));
		}

		Boolean *__eq__ (Integer valor) {
			return new Boolean ((bool) (this->valor == valor.__getcvalue__ ()));
		}

		Boolean *__eq__ (Integer *valor) {
			return new Boolean ((bool) (this->valor == valor->__getcvalue__ ()));
		}

		Boolean *__le__ (int valor) {
			return new Boolean ((bool) (this->valor <= valor));
		}

		Boolean *__le__ (Integer valor) {
			return new Boolean ((bool) (this->valor <= valor.__getcvalue__ ()));
		}

		Boolean *__le__ (Integer *valor) {
			return new Boolean ((bool) (this->valor <= valor->__getcvalue__ ()));
		}

		Boolean *__ge__ (int valor) {
			return new Boolean ((bool) (this->valor >= valor));
		}

		Boolean *__ge__ (Integer valor) {
			return new Boolean ((bool) (this->valor >= valor.__getcvalue__ ()));
		}

		Boolean *__ge__ (Integer *valor) {
			return new Boolean ((bool) (this->valor >= valor->__getcvalue__ ()));
		}

		Boolean *__lt__ (int valor) {
			return new Boolean ((bool) (this->valor < valor));
		}

		Boolean *__lt__ (Integer valor) {
			return new Boolean ((bool) (this->valor < valor.__getcvalue__ ()));
		}

		Boolean *__lt__ (Integer *valor) {
			return new Boolean ((bool) (this->valor < valor->__getcvalue__ ()));
		}

		Boolean *__gt__ (int valor) {
			return new Boolean ((bool) (this->valor > valor));
		}

		Boolean *__gt__ (Integer valor) {
			return new Boolean ((bool) (this->valor > valor.__getcvalue__ ()));
		}

		Boolean *__gt__ (Integer *valor) {
			return new Boolean ((bool) (this->valor > valor->__getcvalue__ ()));
		}

		Boolean *__not__ () {
			return (this->valor == 0) ? new Boolean (true) : new Boolean (false);
		}

		// Metodos estáticos
		static Integer *parseInt (const char *frase) {
			bool esEntero = true;
			for (int i = 0; i < strlen (frase); i++)
				if (!(*(frase + i) < ':' && *(frase + i) > '/'))
					esEntero = false;

			if (esEntero) {
				return new Integer (atoi (frase));
			} else {
				printf ("\033[31;40mEl parámetro pasado a Integer::parseInt() contiene caracteres no numericos.\n\033[97;40m]]");
				exit (EXIT_FAILURE);
			}
		}

		// List *__mro__ () {
		// 	List *ret = Object::__mro__ ();
		// 	ret->append (new String (this->__name__ ()));
		// 	return ret;
		// }

	private:
		long long int valor;
};

#endif