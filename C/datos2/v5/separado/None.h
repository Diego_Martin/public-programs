#ifndef __NONE_H__
#define __NONE_H__

#include "Object.h"
#include "List.h"
#include <stdlib.h>

class None : public Object
{
	public:
		None () {
			this->valor = NULL;
		}

		~None () {}

		const char *__repr__ () {
			return "None";
		}

		const char *__name__ () {
			return "None";
		}

		unsigned long long int __size__ () {
			return sizeof (this->valor);
		}

		Object * __call__ () {
			Object *res (NULL);
			return res;
		}

		static None *nada () {
			return new None ();
		}

		None *__cvalue__ () {
			return this;
		}

		// List *__mro__ () {
		// 	List *ret = Object::__mro__ ();
		// 	ret->append (new String (this->__name__ ()));
		// 	return ret;
		// }

	private:
		void *valor;
};

#endif