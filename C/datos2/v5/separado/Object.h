#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

class Object
{
	public:
		Object () {
			if (strcmp (this->__name__ (), "Object") == 0) {
				this->__doc__ = "La clase base de la jerarquía de clases.\n\nCuando es llamada, no acepta argumentos y retorna una nueva instancia sin características\nque no tiene atributos ni se le pueden dar.";
			} else {
				if (this->__doc__ == NULL) {
					char *res = (char *) malloc (sizeof (char) * (29 + strlen (this->__name__ ())));
					strcpy (res, "Documentacion de la clase: ");
					strcat (res, this->__name__ ());
					strcat (res, ".\n");
					this->__doc__ = (const char *) malloc (sizeof (char) * strlen (res));
					this->__doc__ = (const char *) res;
				}
			}
		}

		virtual const char *__name__ () {
			return "Object";
		}

		virtual const char *__repr__ () {
			char *res = (char *) malloc (sizeof (char) * (12 + strlen (this->__name__ ()) + 14));
			sprintf (res, "<clase %s en %p>", this->__name__ (), (void *) this);
			return (const char *) res;
		}

		virtual const char *__str__ () {
			char *res = (char *) malloc (sizeof (char) * (42 + strlen (this->__name__ ())));
			sprintf (res, "<ranura extra '__str__' de los objetos '%s'>", this->__name__ ());
			return (const char *) res;
		}

		virtual unsigned long long int __sizeof__ () {
			return sizeof (*this);
		}
		/*======================================================
		=            Uso y proposito de este metodo            =
		======================================================*/
		/*
			Para los tipos de datos compuestos hacer un 
			dynamic_cast <[Objeto en cuestion]->__getcvalue__()>
		*/
		/*=====  End of Uso y proposito de este metodo  ======*/
		
		Object *__cvalue__ () {
			return this;
		}

		const char *__doc__ = NULL;

		// List *__mro__ () {
		// 	List *ret = new List ();
		// 	ret->append (new String (this->__name__ ()));
		// 	return ret;
		// }

		// Dict *__base__ () {
		// 	Dict *ret = new Dict ();
		// 	// Tantas veces como clases madre tenga
		// 	// ret->update (new String (Object::__name__ ()), new Object (NULL));
		// 	return ret;
		// }
};

#endif