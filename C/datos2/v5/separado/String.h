#ifndef __STRING_H__
#define __STRING_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Object.h"
#include "Measurable.h"
#include "Boolean.h"
#include "Integer.h"
#include "None.h"

class String : public Object, public Measurable
{
	public:
		String () {
			this->__setstr__ ("");
		}

		String (const char *texto) {
			this->__setstr__ (texto);
		}

		String (char *texto) {
			this->__setstr__ ((const char *) texto);
		}

		String (char texto) {
			this->res = (char *) malloc (sizeof (char) * 2);
			sprintf (res, "%c", texto);
			this->__setstr__ ((const char *) this->res);
		}

		~String () {
			free (this->valor);
			free (this->res);
		}

		const char *__name__ () {
			return "String";
		}

		const char *__repr__ () {
			return (const char *) this->valor;
		}

		const char *__str__ () {
			this->res = (char *) malloc (sizeof (char) * (strlen (this->valor) + 2));
			sprintf (res, "'%s'", this->valor);
			return (const char *) this->res;
		}

		void __setstr__ (const char *frase) {
			this->valor = (char *) malloc (sizeof (char) * strlen (frase));
			strcpy (this->valor, frase);
		}

		unsigned long long int __len__ () {
			return (unsigned long long int) strlen (this->valor);
		}

		int __size__ () {
			return sizeof (*this);
		}

		String *__hash__ (int index) {
			return new String (this->get (index));
		}

		String *__hash__ (Integer index) {
			return new String (this->get (index.__getcvalue__ ()));
		}

		String *__hash__ (Integer *index) {
			return new String (this->get (index->__getcvalue__ ()));
		}

		int length () {
			return strlen (this->valor);
		}

		String *__add__ (String *valor) {
			stringstream canal;
			canal << this->__repr__ () << valor->__repr__ ();
			const string& res = canal.str ();
			return new String (res.c_str ());
		}

		String *__add__ (String valor) {
			return this->operator+ (&valor);
		}

		String *__add__ (const char *valor) {
			return this->operator+ (new String (valor));
		}

		String *__add__ (const String& valor) {
			return this->operator+ ((String *) &valor);
		}

		None *__as__ (String *valor) {
			this->__setstr__ (valor->__repr__ ());
			return new None ();
		}

		None *__as__ (String valor) {
			this->operator= (&valor);
			return new None ();
		}

		None *__as__ (const char *valor) {
			this->operator= (new String (valor));
			return new None ();
		}

		None *__as__ (const String& valor) {
			this->operator= ((String *) &valor);
			return new None ();
		}

		String *__mul__ (int multiplicador) {
			if (multiplicador == 0)
				return new String ();

			stringstream canal;
			canal << "";

			for (int i = 0; i < multiplicador; i++)
				canal << this->__repr__ ();
			
			const string& res = canal.str ();
			return new String (res.c_str ());
		}

		String *__mul__ (Integer multiplicador) {
			return this->__mul__ (multiplicador.__getcvalue__ ());
		}

		String *__mul__ (Integer *multiplicador) {
			return this->__mul__ (multiplicador->__getcvalue__ ());
		}
	private:
		char *valor;
		char *res;

		char get (int index) {
			if (index > -1) {
				if (index < this->length()) {
					char res = *(this->valor + index);
					return res;
				} else {
					printf ("\033[31;40mERROR: El indice debe ser menor que la longitud de la palabra.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			} else {
				if (index > (-1 * this->length())) {
					char res = *(this->valor + this->length() + index);
					return res;
				} else {
					printf ("\033[31;40mERROR: El indice debe ser mayor que la longitud de la palabra en negativo.\n\033[97;40m");
					exit (EXIT_FAILURE);
				}
			}
		}

};


#endif