#ifndef __MEASURABLE_H__
#define __MEASURABLE_H__

#include "Object.h"
#include "Integer.h"

class Measurable
{
	public:
		virtual unsigned long long int __len__ () = 0;
		virtual Object *__hash__ (Object *index) = 0;
		virtual Object *__slice__ (Integer *start, Integer *end = new Integer (-1), Integer *step = new Integer (1)) = 0;
		unsigned long long int len;
};

#endif