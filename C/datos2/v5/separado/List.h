#ifndef __LIST_H__
#define __LIST_H__

#include "Object.h"
#include "Measurable.h"
#include "Integer.h"
// #include "IndexOutOfBoundsException.h"
#include "None.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
using namespace std;

class List : public Object//, public Measurable
{
	public:
		List () {
			this->len = 0;
			this->valor = (Object **) malloc (sizeof (Object *) * this->len);
		}
		~List () {
			free (this->valor);
		}

		const char *__repr__ () { // DONEEEE
			stringstream canal;
			canal << "[";
			for (int i = 0; i < this->len; i++) {
				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";

				canal << this->operator[] (i)->__repr__ ();

				if (strcmp (this->operator[] (i)->__name__(), "String") == 0)
					canal << "'";
				
				if (i < this->len - 1)
					canal << ", ";
			}
			canal << "]";
			const string& res = canal.str ();
			char *ret = (char *) malloc (sizeof (char) * strlen (res.c_str ()));
			strcpy (ret, res.c_str ());
			return (const char *) ret;
		}

		const char *__name__ () {
			return "List";
		}

		unsigned long long int __sizeof__ () {
			return sizeof (Object *) * this->len;
		}

		unsigned long long int __len__ () {
			return this->len;
		}

		// Object *operator [] (int index) {
		// 	if (index < this->len && index > -1)
		// 		return *(valor + index);
		// 	else if (index >= (-1 * this->len) && index < 0)
		// 		return *(valor + (-1 * index));
		// 	else
		// 		throw new IndexOutOfBoundsException (index, this->__len__ ());
		// }

		Object *__hash__ (int index) {
			if (index < this->len && index > -1)
				return *(valor + index);
			else if (index >= (-1 * this->len) && index < 0)
				return *(valor + (-1 * index));
			else
				throw 2;//new IndexOutOfBoundsException (index, this->__len__ ());
		}

		Object *__hash__ (Integer *index) {
			return &this [(int) index->__getcvalue__ ()];
		}

		None *append (Object *elemento) {
			this->__grow__ ();
			*(this->valor + this->len - 1) = elemento;
			return new None ();
		}

		None *append (Object *elemento, Object *elemento2) {
			this->__grow__ (2);
			*(this->valor + this->len - 2) = elemento;
			*(this->valor + this->len - 1) = elemento2;
			return new None ();
		}

		// List *__mro__ () {
		// 	List *ret = Object::__mro__ ();
		// 	ret->append (new String (this->__name__ ()));
		// 	return ret;
		// }

	private:
		Object **valor;
		unsigned long long int len;

		void __grow__ (int size = 1) {
			this->len += size;
			this->valor = (Object **) realloc (this->valor, sizeof (Object *) * this->len);
		}
};

#endif