#include <stdio.h>

void hola () {
    printf ("Hola\n");
}

void (*decir ())() {
    return hola;
}

int main () {
    decir ()();
    return 0;
}