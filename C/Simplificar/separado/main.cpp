#include "Sistema.h"
#include "Navegador.h"




int main(int argc, char const *argv[])
{
	Sistema.Consola.EscribirLinea("EscribirLinea");
	Sistema.Consola.Limpiar(stdout);
	Sistema.Consola.SetLimpiar(true);
	Sistema.Consola.SetColorFondo(Sistema.Consola.Fondos.AzulOscuro);
	Sistema.Consola.SetColorTexto(Sistema.Consola.Colores.AmarilloClaro);
	Sistema.Consola.EscribirLinea("%i", Sistema.Tiempo.TodosLosSegundos());
	Sistema.Consola.SetColorConsola(Sistema.Consola.Colores.AmarilloClaro, Sistema.Consola.Fondos.NegroOscuro);
	Sistema.Consola.EscribirLinea("Hora completa: %s", Sistema.Tiempo.GetHoraInicioDeProgramaStr("H:m:s"));
	Sistema.Consola.EscribirLinea("Hora: %i", Sistema.Tiempo.Reloj.GetHora());
	Sistema.Consola.EscribirLinea("Minuto: %i", Sistema.Tiempo.Reloj.GetMinuto());
	Sistema.Consola.EscribirLinea("Segundos: %i", Sistema.Tiempo.Reloj.GetSegundos());
	Sistema.Consola.EscribirLinea("Sistema operativo: %s", Sistema.GetSO());
	Sistema.Consola.EscribirLinea("Fecha: %s", Sistema.Tiempo.GetFechaInicioDeProgramaStr("dd-MM-YYYY"));
	// Sistema.Ejecutar("toilet -fpagga --gay Hola");
	// Sistema.Sonar ();
	// Navegador.SetRutaFirefox ("/usr/lib/firefox/firefox");
	Navegador.Firefox.SetRutaFirefox ("\"C:/Program Files/Mozilla Firefox/firefox.exe\"");
	Sistema.Consola.EscribirLinea ("Ha llegado hasta aqui");
	Navegador.Firefox.Abrir ("www.youtube.com");
	Sistema.Tiempo.Esperar (3.5);
	Navegador.Firefox.AbrirNuevaPestania ("www.google.es");
	Sistema.Tiempo.Esperar (2);
	Navegador.Chrome.SetRutaChrome ("\"C:/Program Files (x86)/Google/Chrome/Application/chrome.exe\"");
	Navegador.Chrome.Abrir ("https://htmlcolorcodes.com/es/");
	// Sistema.Consola.EscribirLinea("2 ^ 5 = %lli", Sistema.Matematicas.Potencia<Sistema.Matematicas.Retorno> (2, 5));
	
	Sistema.Salir(Sistema.SALIR_EXITO);
}