#ifndef __NAVEGADOR_H__
#define __NAVEGADOR_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Sistema.h"

class FFirefox
{
	private:
		const char *Ruta;
	public:
		FFirefox() {
			Ruta = "";
		}
		int SetRutaFirefox (const char *ruta = "") {
			if (this->Ruta = ruta)
				return 1;

			printf ("\033[31;40m");
			printf ("Se ha producido un error al fijar la ruta del ejecutable del navegador.");
			printf ("\033[37;40m");
			exit (EXIT_FAILURE);
		}

		int Abrir (const char *url = "https://www.google.es/") {
			if (Ruta == "") {
				printf ("\033[31;40m");
				printf ("Aun no se ha fijado la ruta absoluta del ejecutable del navegador.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			char *comando = NULL;
			#ifdef MiWindows
				comando = (char *) malloc (sizeof (char) * (strlen (Ruta) + strlen (url) + 13));
				sprintf (comando, "%s -new-window %s", Ruta, url);
			#elif defined MiLinux
				comando = (char *) malloc (sizeof (char) * (strlen (Ruta) + strlen (url) + 14));
				sprintf (comando, "%s --new-window %s", Ruta, url);
			#endif
			system ((const char *) comando);
			return 1;
		}
		int AbrirNuevaPestania (const char *url = "https://www.google.es/") {
			if (Ruta == "") {
				printf ("\033[31;40m");
				printf ("Aun no se ha fijado la ruta absoluta del ejecutable del navegador.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			}
			char *comando = NULL;
			#ifdef MiWindows
				comando = (char *) malloc (sizeof (char) * (strlen (Ruta) + strlen (url) + 10));
				sprintf (comando, "%s -new-tab %s", Ruta, url);
			#elif defined MiLinux
				comando = (char *) malloc (sizeof (char) * (strlen (Ruta) + strlen (url) + 11));
				sprintf (comando, "%s --new-tab %s", Ruta, url);
			#endif
			system ((const char *) comando);
			return 1;
		}
		// ~FFirefox();
};

class CChrome
{
	private:
		const char *Ruta;
	public:
		CChrome() {
			Ruta = "";
		}
		int SetRutaChrome (const char *ruta = "") {
			if (this->Ruta = ruta)
				return 1;

			printf ("\033[31;40m");
			printf ("Se ha producido un error al fijar la ruta del ejecutable del navegador.");
			printf ("\033[37;40m");
			exit (EXIT_FAILURE);
		}

		int Abrir (const char *url = "https://www.google.es/") {
			if (Ruta == "") {
				printf ("\033[31;40m");
				printf ("Aun no se ha fijado la ruta absoluta del ejecutable del navegador.");
				printf ("\033[37;40m");
				exit (EXIT_FAILURE);
			} 
			char *comando = NULL;
			#ifdef MiWindows
				comando = (char *) malloc (sizeof (char) * (strlen (url) + 13));
				sprintf (comando, "%s %s", Ruta, url);
			#elif defined MiLinux
				// comando = (char *) malloc (sizeof (char) * (strlen (Ruta) + strlen (url) + 14));
				// sprintf (comando, "%s --new-window %s", Ruta, url);
				printf("No programado aun\n");
			#endif
			system ((const char *) comando);
			#ifdef MiWindows
				free (comando);
			#endif
			return 1;
		}
		int AbrirNuevaPestania (const char *url = "https://www.google.es/") {
			this->Abrir (url);
			return 1;
		}
		// ~CChrome();
};

class NNavegador
{
	private:
	public:
		// NNavegador() {}
		// ~NNavegador();
		FFirefox Firefox;
		CChrome Chrome;
};

NNavegador Navegador;

#endif