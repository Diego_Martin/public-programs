#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	/* code */
	int uno = 1;
	int dos = 2;

	(dos ^= uno), (uno ^= dos), (dos ^= uno);

	printf("uno = %i\n", uno);
	printf("dos = %i\n", dos);
	return 0;
}