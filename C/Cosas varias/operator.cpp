#include <stdlib.h>

class Hash
{
	public:
		Hash () {
			valores[0] = 1;
			valores[1] = 2;
			valores[2] = 3;
			valores[3] = 4;
			valores[4] = 5;
			valores[5] = 6;
		}
		~Hash () {
			free (this->valores);
		}
		int *operator [] (int start, int end) {
			int res[(end - start)];
			for (int i = start, j = 0; i < end; i++, j++) {
				res [j] = this->valores [i];
			}
			return res;
		}
	private:
		int valores [6];
};

int main(int argc, char const *argv[])
{
	Hash ejemplo ();
	return 0;
}